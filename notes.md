# Personal notes

### 10/06/20

`Actor` is the lowest level of `Object` that can be placed in the world, while `Pawn` is the lowest level of controllable `Actor` that you can either posses as a `PlayerController` or an `AIController`.

[Scatterer](https://www.stateofartacademy.com/en/ue4-custom-scatterer/)

[Magnetic Field Implementation](https://www.youtube.com/watch?v=--xR0aQ2rB0&ab_channel=Lusiogenic)

[Heat Transfer Formula](https://www.toppr.com/guides/physics-formulas/heat-transfer-formula/)

[Equilibrium temperature of two bodies](https://phas.ubc.ca/~kiefl/ch15_part2.pdf)

[Time to reach thermal equilibrium](https://physics.stackexchange.com/questions/248202/calculating-the-time-for-two-bodies-to-reach-thermodynamic-equilibrium)

I'll make a temperature component that will be added to actors / pawns / characters. This component will have a temperature attribute, a mass and a specific heat. Or maybe the mass will be of the owning actor, I'll see.

To simulate heat exchange between two bodies I can do something similar to the magnetic field implementation. To simulate the ambience, i.e. a thermostat, I'll have a scatterer of point-like actors with the component on them.

### 10/08/20

The `UTemperatureComponent` works, but `GetOverlappedComponents()` seems to be working only on certain conditions:
- Custom actor BP vs basic actor (e.g. Cube) : ok
- Custom actor NP vs custom actor : ok
- Basic actor vs basic actor : no

I don't get why it's like this. I tried changing some the collision preset of the mesh, thinking that maybe that was hiding the one of the capsule component, but nothing to do.  
Maybe it's because it is a c++ component? Maybe by creating a custom actor with c++ those component would solve my problems?
Next time: write a custom actor in c++ that has the temperature component

### 10/09/20

After countless tries with default cube and sphere actors, I gave up and created my own actor c++ class. Nothing special: capsule for collision, mesh, temperature component. It works :rofl: :tada: . I still have no idea why it doesn't work with the default actor classes, but whatever.

### 10/10/20

I came up with a frame-by-frame formula, I tested it and that I think works pretty well. I wrote my procedure in the `Heat_transmission_formula.pdf` file.

### 10/11/20

Working on changing the actor's material color with temperature. 
- For the code, I followed the first 10 minutes of [this BP tutorial](https://www.youtube.com/watch?v=6OTaEHfRyH8&ab_channel=DeanAshford), translating everything to C++. Essentially: I created a custom material with a vector parameter called "Color", assigned it to the `AActorTemperatureBase`, and created an instance of the material at `BeginPlay` (so that each instance of the actor has a different material).

- For the formula, after defining a min and max temperature on the `UTemperatureComponent`, I used the following to map the temperature of the actor into a value between 0 and 1:
  ```cpp
  (TemperatureComp->GetTemperature() - UTemperatureComponent::MinTemperature) / (UTemperatureComponent::MaxTemperature - UTemperatureComponent::MinTemperature)
  ```
  Initially, I assigned this value to the red channel, while the blue channel gets 1 - this value; green and alpha channels were 0 and 1 respectively.  
  I didn't like this though, because not using the green channel implies a transition through violet / purple that doesn't give an idea of "hot and cold" as colors like cyan, yellow and orange do. Therefore, I made a more complex mapping that forces passing by cyan and yellow (see `AActorTemperatureBase::SetColorWithTemperature()`). This mapping can be made way more complex, by fixing more intermediate values and / or changing the width of the various intervals (for example, I can force cyan to correspond to zero celsius, orange to 500 celsius, etc). I'll probably end up refining this in the future, for now it's ok though.

### 10/13/20

Take a look [here](https://www.youtube.com/watch?v=oMIbV2rQO4k&ab_channel=TechArtAid) for an explanation on why, when you have a lot of identical meshes to instantiate, the `UHierarchicalInstancedStaticMeshComponent` is the way to go.

Even though I have to instantiate identical _actors_, not _meshes_, today I experimented a bit with this object. It's incredible how terrible my actual pc is: even though hierarchical instanced meshes are praised for how light they are on the CPU and used to place thousands of identical meshes without problems, on my pc Unreal start lagging very hard with just 8 base sphere meshes with the same material being spawned.

This is worrying: my temperature actor (collision capsule + static mesh + temperature component) is much heavier than a simple static spherical mesh. Besides, there isn't an equivalent of `UHierarchicalInstancedStaticMeshComponent` for actors: they have to be spawned one by one. Of course, it's just at construction, not at runtime, but... besides, if my pc is lagging for just 8 spheres being moved on the sphere, what the hell will happen when I press play and all the actors I placed with the scatterer start exchanging temperature? Maybe I can gain something by disabling collisions except for the heat channel (at the link above he says something around the end of the video), but still...

Unfortunately, I am afraid I won't be able to go much further without a better pc with an actual GPU.

### 10/14/20

Ok, for some reason spawning actor is way lighter than spawning meshes. It's probablly because `UHierarchicalInstancedStaticMeshComponent` is powerful only when the PC has a serious GPU, and it's not my case.

My current problem is that, apparently, spawning actors on construction is really really hard and dangerous, everybody recommends not to do it. I could simply spawn them at beginplay and use something like a [debug point](https://docs.unrealengine.com/en-US/API/Runtime/Engine/DrawDebugPoint/index.html) during OnConstruction, I'll see.

### 10/15/20

Super useful: [Actor lifecycle](https://docs.unrealengine.com/en-US/Programming/UnrealArchitecture/Actors/ActorLifecycle/index.html)

Ok, thanks to the `Destroyed()` method I managed to solve that weird bug that caused the last molecules before scatterer deletion to stay on the level.

Ok, air simulation works, at least for not many air molecules on the map. Problem: I set the area to one in the heat transmission formula and I am not accounting for the mass in the heat capacity, effectively treating it as specific heat capacity -> I am completely ignoring the size and mass of objects! This causes unrealistic behaviours, like a giant cold ball that essentially reaches the initial temperature of hot air.

To do:
- search for air's specific heat capacity, mass of 1m^3 of air, and determine heat capacity per m^3. Find a way to get actor's mass (mass is a property already defined by Unreal)
- think of a way to account for dimensions somehow... it would be cool to determine the area of intersection for two capsules, but It's probablly a very complicated formula and not worth it. Maybe something like a cylinder whose base is the smallest of the two temperature capsule's circles? Or something in between. But this only works if the capsule is completely spherical.. :thinking:
- make it possible to hide / show air molecules while the game is going

### 10/17/20

Tons of bugs at engine startup. Let's solve them one at a time:

#### Bug 1: AActorTemperatureBase, Temperature and Mesh components exchanged at startup

If I place an `AActorTemperatureBase` on the map, it behaves as it should: it has a `UTemperatureComponent` and a `UStaticMeshComponent` attached to it. But if I close the engine and then re-open it, the two components are exchanged: now the mesh is the root component, and the `UTemperatureComponent` is attached to it. This causes a very weird behaviour when I press the play button, and the warning message "Root component cannot be attached to other components in the same actor. Aborting.", which makes sense since `UTemperatureComponent` is my root component.  
This didn't happen before the scatterer MR, meaning that I broke something in the `UTemperatureComponent` or in the `AActorTemperatureBase` with my latest changes.

The `BP_AirMolecule`, which is also a BP sublcass of `AActorTemperatureBase` like `BP_TemperatureSphere`, doesn't have this kind of problem: its components aren't exchanged when I reload the engine. 

I created a new `BP_TemperatureSphere` class and the problem disappeared. I think this was a hot reload fail of the engine: during the MR for the scatter, I was doing some tests on `AActorTemperatureBase` and, at some point I had, the Mesh as RootComponent and The TempComp attached to it. Then I decided to switch them, making the TempComp the new RootComponent. The engine probablly didn't register this properly for the BP derived classes, but deleting the old BP derived class and making a new one solved the problem.

#### Bug 2: AActorTemperatureScatterer, reload problems

When I reload the engine, if the TemperatureScatterer is on the map and I hit play:
1. The temperature values of his molecules are NaN
2. If I move the scatterer around, the initial air molecules aren't deleted

This is because the `TArray<AActorTemperatureBase*>` that the scatterer has isn't saved, so the scatterer loses ownership of its molecules while they remain on the map. They can't therefore receive their initial temperature value from it, nor be deleted.

A solution to this problem could be finding a way to destroy all the air molecules when the map is closed and recreate them when the map is opened.

Another solution could be adding the MoleculeScatterer instead of just setting this object as their owner. [This](https://answers.unrealengine.com/questions/167742/set-the-actors-owner-in-c.html) could help. There is also a `AttachToComponent` function that I could use to attach the molecules to the scatterer box component. This way, the molecules would be "saved" into the scatter and not lost during engine restarts. With `DetachFromActor` I would undo this. Essentially, `OnConstruction`:
1. I call `DetachFromActor` (needed because, if the step or box dimensions change, the actor number increases / decreases, therefore I need to get rid of the old molecules and restart from the beginning)
2. I cycle through all the lattice and create new molecules and attach them with `AttachToComponent`

This is very expansive computationally and might still have some issues I am unaware of. Besides, [here](https://medium.com/@alexandre.lombard_91611/be-careful-with-the-construction-script-ue4-26991991a88d) is an article on how the construction script should be used. The construction script is called:
1. When the actor is spawned
2. When the actor is moved
3. When a property of the actor is changed
The `OnConstruction()` script should be kept tiny and fast, which is why everybody says it's bad practice to use it to spawn actors (it's also very dangerous, I dealt with a few crashes in the last days while working on the scatterer). 

For the reasons above, I decided to give up on spawning `BP_AirMolecule` instances during `AActorTemperatureScatterer::OnConstruction()`. I'll instead:
1. Use `OnConstruction` to draw debug points that give me an idea where the air molecules will be spawned. This is enough for me, I don't need to see the AirMolecules actor on the editor, a simple debug sphere or point is enough :) 
2. Use `BeginPlay` to spawn the real air molecules exactly like I am doing now during `OnConstruction`.

This approach will make `BeginPlay` slower, but should make `OnConstruction` faster while solving the bug.

### 10/18/20

Another day, another bug. I am trying to close [this issue](https://gitlab.com/LucaPedrelli/scalarfield/-/issues/7)

I started by looking at [this link](https://answers.unrealengine.com/questions/66815/how-to-handle-property-updates-in-code.html). To get the value that was inserted, look [here](https://answers.unrealengine.com/questions/139582/get-property-value.html) and [here](https://answers.unrealengine.com/questions/190937/read-variable-value-from-uproperty-of-different-ty.html).

The bug that causes the scatterer initial temperature to not be read by spawned molecules persists: `PostEditChangeProperty()` works only for editor changes, i.e. calling `UTemperatureComponent::SetInitialTemperature` from `AActorTemperatureScatterer::MoleculesCubicScattering` doesn't trigger `UTemperatureComponent::PostEditChangeProperty()`.

I opted for the easiest solution: make `UTemperatureComponent::SetInitialTemperature` set `Temperature=InitialTemperature`. At the moment I don't think this will give me problems in the future, but we'll see.

#### Mass and Heat Capacity

Real world: a body is made of many different materials. Each material has a specific heat capacity and a mass, which leads to each material having its own heat capacity. Consequence: different parts of the body heat up at different rates.

Obviously, here we just want an approximation... 
1. I could create a custom `UStaticMeshComponent` that has a `SpecificHeatCapacity` field. Component mass is computed automatically by Unreal once I set up the density of its `UPhysicalMaterial` and can be read and set easily. I could also create a custom `UPhysicsMaterial` with specific heat capacity, that's even better probablly... I should study this component, search how it's used etc...
2. On `AActorTemperatureBase` I should create a function to get an `OverallHeatCapacity` of the actor by taking an average of the heat capacities of the various components of the actor. Of course, in the real world there is no "overall heat capacity" of a body, this is my approximation for the game: the various parts of the actor do not heat at different rates, the actor heats at a rate that is determined by the average of the heat capacities of the various components it is made of.
3. `UTemperatureComponent::HeatTransmission` reads the overall capacities of the components owners and uses them in the formula.

## 10/25/20

[PhysicsMaterial HowTo Guide](https://docs.unrealengine.com/en-US/Engine/Physics/PhysicalMaterials/HowTo/index.html)

Once a physical material is created from the editor, it can be assigned to:
- a material
- a material instance
- a static mesh

This is very easy to do, the link above details the procedure.  
There is a surface type attribute that can be added to the physical material but it's only used for flags (play different sound when walking on grass etc etc), not useful right now.

The problem is making the function to have the material interact with the components

### Path to obtain mass

1. `UPrimitiveComponent::GetMass()` is defined in `PrimitiveComponentPhysics.cpp`. Inside this function, we have an iteration through the `FBodyInstance`s of the component to call the `FBodyInstance::GetBodyMass()` function.
2. An `FBodyInstance` is "a container for a physics representation of an object" (cit. UE4 documentation). `FBodyInstance::GetBodyMass()` looks like this:
  ```cpp
  float FBodyInstance::GetBodyMass() const
  {
    float OutMass = 0.0f;

    FPhysicsCommand::ExecuteRead(ActorHandle, [&](const FPhysicsActorHandle& Actor)
    {
      OutMass = FPhysicsInterface::GetMass_AssumesLocked(Actor);
    });

    return OutMass;
  }
  ```
  You could go even deeper, into `FPhysicsInterface::GetMass_AssumesLocked`, but it's insane don't do it. Just... don't.

### Action plan

1. Create a custom `UThermalStaticMeshComponent` from `UStaticMeshComponent` which has a `SpecificHeat` attribute
    - This component has a mass too since it's a child of `UPrimitiveComponent`, hence I can call
`GetMass()` on it
2. Make a function of `AActorTemperatureBase` which computes an average Heat Capacity for the actor
3. The Heat Capacity of the owner is read from `UTemperatureComponent` and used in the `HeatTransmission` function

This approach is nice because:
- It's not too complex (making a custom `UPhysicalMaterial` is easy but making it interact with components is a total nightmare)
- An actor can have static mesh components of different mass and materials, i.e. different heat capacity, attached to it, simulating rpg equipment. Each piece of equipment contributes to the actor's average heat capacity in a different way: having metal armor and silk trousers is different from having metal armor and metal trousers, and so on. I can have materials, e.g. silk, that do not protect much from piercing/slashing/bludgeoning damage, but they're harder to heat up / cool down, offering resistance to heat magic.

## 10/28/20
Steps 1 and 2 of the action plan completed. Next steps:
- I need to solve the "has to have simulate physics enabled if you'd like to GetMass()" bug :bug:. According to [this discussion](https://answers.unrealengine.com/questions/720542/has-to-have-physics-enabled-if-youd-like-to-getmas.html) this is an unreal bug and cannot be solved :thinking: I tried to `SetSimulatePhysics(true)` in the component constructor, but this way the actors are affected by gravity and I don't want that. You have to play a bit with SetEnableGravity and SetSimulatePhysics...
- I need to tackle step 3

## 11/03/20

The problem with gravity and mass has been solved. Now, before proceeding with step 3, I'd like to find a way to set the heat capacity at actor spawn without having to change the specific heat capacity. The problem is that GetMass() returns 0 if I call it in the constructor, in PostInitProperties(), in OnComponentCreated() and in PostLoad()... sad reacts only.

If I can't find a way to get the mass from the editor, I have two choices:
1. set the specific heat capacity manually for each class (but then the mass value is different from the value of the instances somehow hence the starting heat capacity is different)
2. Delete the specific heat capacity attribute and only have the heat capacity one, that is set directly by the user. Not physically accurate? Don't care. I am going insane here.

## 11/04/20

SpecificHeatCapacity deleted, now only HeatCapacity is a property of the `HeatCapacityStaticMeshComponent`; it's a shame, but it is what it is, I am tired of fighting with the engine.

Now I must employ the `AverageHeatCapacity` of the TemperatureActors in the `UTemperatureComponent::HeatTransmission`.

I decided to fix the distance in the heat transmission formula to 1cm. This choice has been made because the actor distance is an awful approximation for actors with very large meshes and an accurate calculation of the distance between two meshes surfaces would require a lot of math and still be inaccurate to a certain degree.  
However, this shouldn't be a big problem: because of how I am implementing the heat transmission, i.e. collision between specific components, If I keep the temperature components only a bit bigger than the actor's mesh, two component exchange heat only if they're very very close. Air molecules are the only actors that have a temperature component significantly bigger than the mesh.

[Air thermal conductivity](https://en.wikipedia.org/wiki/Thermal_conductivity)

"ERR: GetAverageHeatCapacity is not a member of AActor" made me think about including `AActorTemperatureBase` into the temperature component; but then I thought: wouldn't this create a cyclic include statement?  
Average heat capacity should probablly be on the temperature component, not the AActorTemperatureBase; moving `SetAverageHeatCapacity()` + the attribute from the actor to the temperature component could fix this?

## 11/05/20

> moving `SetAverageHeatCapacity()` + the attribute from the actor to the temperature component could fix this?

HAHAHAHAHAHAHAHA, you fool! `SetAverageHeatCapacity()` is called during actor's `OnConstruction()`, but there is no such method for components! 

Setup:
- `AverageHeatCapacity` is a `UTemperatureComponent` attribute, a setter method
- the setter is called from the owning actor at `OnConstruction()`
- in HeatTransmission(), tick by tick, we read the attribute value

## 11/08/20

Making the player RTS-like pawn class. I already did something like this in [TablesAndChairs](https://github.com/PedrelliLuca/TableAndChairs/blob/master/Source/TableAndChairs/Private/TC_Camera.cpp), I'll take inspiration from there. For the key bindings, I am taking inspiration from [DA: Origins controls](https://dragonage.fandom.com/wiki/Controls_(Origins))

Once I created this class, I'll use it to read action and axis mappings. The first thing I'd like to do with these is to turn molecules visibility on and off by `USceneComponent::SetHiddenInGame()` on each molecule spawned by the scatterer actor.

## 11/09/20

Trying to resolve a ridiculous bug explained [here](https://community.gamedev.tv/t/bp-forgets-c-parent-after-engine-reload-cant-reparent-to-it/150357).

Solution: PlayerCamera apparently isn't a valid name. Why? I don't know. The engine doesn't stop you from using it when you create the class. The class has been renamed to PawnPlayerCamera.

## 11/11/20

Today I effectively start working on the air molecules visibility thing.

## 11/12/20

Refactoring: I'm gonna remove the custom mesh component I made from `AActorTemperatureBase` so that it is as much generic as possible. But before doing that: I want to move the `SetColorWithTemperature()` function from `AActorTemperatureBase` to  `UHeatCapacityStaticMeshComponent`.

After thinking about this for a bit, I got a better idea: leave `SetColorWithTemperature()` on `AActorTemperatureBase` but set the color value on each of the `UHeatCapacityStaticMeshComponent`s it has by cycling through them (the function is called only once, the value is applied to every mesh through a certain function)

This must be moved to the mesh' `BeginPlay()`:
```cpp
void AActorTemperatureBase::BeginPlay() 
{
	Super::BeginPlay();
	if (!Mesh)
		return;
	MaterialInstance = Mesh->UPrimitiveComponent::CreateDynamicMaterialInstance(0, Mesh->GetMaterial(0), TEXT(""));
}
```

The `AActorTemperatureBase` can have an array to store these meshes.

Maybe I could have `AActorTemperatureBase::SetAverageHeatCapacity()` called from the mesh when the specific heat capacity changes :thinking:

## 11/15/20

Trying to solve a bug that makes possible to set negative heat capacity in `UHeatCapacityStatiMeshComponent`. I don't have this problem with the initial temperature in `UTemperatureComponent`, I don't understand why I have it here.

Somehow, the changes I make to `HeatCapacity` aren't registered by the editor while the ones to `InitialTemperature` and `Temperature` are :thinking:

Look [here](https://answers.unrealengine.com/questions/592358/properties-edited-in-posteditchangeproperty-do-not.html)

[This](https://forums.unrealengine.com/development-discussion/c-gameplay-programming/1492410-correct-way-to-show-updates-from-value-changes-in-editor) taught me that the problem might be related to the fact that the construction script of the owning actor runs before the `PostEditChangeProperty` of the attached static mesh components (this can be proven by putting logs in the `OnConstruction` method of `AActorTemperatureBase` and `PostEditChangeProperty` of the heat capacity component). Since `OnConstruction` reads the heat capacity value while `PostEditChangeProperty` changes it, there is probablly some conflict which causes the bug.  
The temperature component doesn't have this problem because the owning actor doesn't read initial temperature during `OnConstruction`. 

However, I tried to comment every line in `AActorTemperatureBase::OnConstruction` that references to the heat capacity somehow and the problem persists...

Other differences between the two components:
1. UTemperatureComponent is the root component of `AActorTemperatureBase`, `UHeatCapacityStaticMeshComponent` isn't
2. `UTemperatureComponent` has been added to `AActorTemperatureBase` via C++, the mesh components via BP.
3. In UTemperatureComponent I use the `FMath::Clamp` function. I already checked this my using clamp in the heat capacity component, no luck.

Other options:
4. Maybe HeatCapacity isn't a valid name for some reason? Nope, changing the name doesn't improve things

I could create a dummy actor class to test if the problems comes from the fact that the mesh components have been added via bp.

Ok the problem was that `Super::PostEditChangeProperty` was called as first action, calling it at the end solved everything. Why? Hahahah, who knows, maybe `UStaticMeshComponent::PostEditChangeProperty` does things differently from `UCapsuleComponent::PostEditChangeProperty`!

## 11/16/20

I want the scatterer to set the heat capacity of the air molecules so that I get the best approximation of real air that is possible. The procedure to do so is explained in issue #15.

To set the heat capacity of the molecules from the scatter I had to create a function in `AActorTemperatureBase` that cycles through all heat capacity components of the actor and sets their heat capacity values to the one computed by the scatterer.  
This kinda goes against the principle I implicitly gave myself that the heat capacity should be set from the component (bottom up) and not from the actor (top down), but I plan to use this function just for the scatterer, so...  
At some point I even thought of creating a c++ class derived from `AActorTemperatureBase` for the molecules since they're starting to have so many special cases... maybe I'll do a huge refactoring in the future, for the moment I'll keep a single c++ class.

The cool part about using this procedure is that if, for example, I have a person at a temperature Tp with heat capacity Cp, immersed in a volume Va of air with density Ra, specific heat capacity c_a and at a temperature Ta, changing the step of the scatterer doesn't change the equilibrium temperature Teq.  
The problem is that, since there are less molecules, the number of heat exchanges is significantly lower. Hence, the higher the step value is, the longer the time to reach Teq gets, making the gameplay less dynamic. Obviusly, bigger step values mean higher loading times at `BeginPlay()`, so it's a pros-cons situation for both cases.  
**Optization should have absolute priority over gameplay** (nobody likes a game that runs at 10fps and takes ages to load, even if it has the best gameplay mechanics ever made), and the best thing for optimization is to have a high step values.   
However, at the same time, **gameplay should have absolute priority over real world behavior** (nobody cares if the temperature of equilibrium is the same of the real world if it takes ages to reach that temperature making the gameplay boring), and when the step value is high the best thing for gameplay is to have air molecules with low heat capacity, even if the density or specific heat capacity values aren't realistic.

I am also quite scared of how long the loading times become when the size of the scatterer increases. Maybe this whole thing of making the scatterer set the molecules heat capacity when I can just manually set it from the BP class of the molecule (avoiding many operations in BeginPlay) is wrong. Maybe this is also valid for the initial temperature: the scatter shouldn't be the one that sets the molecules initial temperature at `BeginPlay()` because I can just set that from the molecule BP. Oh no.  
For the moment, I think I'm going to leave things like I made them, but in the future I should seriously consider the idea of retracing my steps and remove the following two lines from the scatterer:
```cpp
// AActorTemperatureScatterer::MoleculesCubicScattering() 
// These are cool, for sure, but they are a heavy blow to the performances
NewMolecule->SetActorInitialTemperature(MoleculesInitialTemperature);
NewMolecule->SetHeatCapacity(MoleculeHeatCapacity);
```
I should also consider completely removing the debug sphere option `OnConstruction`, it's useless.

I also noticed a bug: sometimes, the upper-right molecules start hotter than the others. Why? :thinking: It's something that depends on the specific heat capacity of the scatterer and the step: lowering these parameters makes the bug much more evident. I fear this has to do with the fact that some molecules starts exchanging heat before others are spawned. I thought `BeginPlay()` didn't allow actors to tick before it's over, but I'm probablly wrong :thinking: Not setting the init temperature and the heat capacity on `BeginPlay()` would help, for sure, but I think this can be fixed via some unreal function that makes bCanEverTick true only after BeginPlay. I'll study a solution.

## 11/18/20

Last time I said:

> For the moment, I think I'm going to leave things like I made them, but in the future I should seriously consider the idea of retracing my steps and remove the following two lines from the scatterer:
> ```cpp
> // AActorTemperatureScatterer::MoleculesCubicScattering() 
> // These are cool, for sure, but they are a heavy blow to the performances
> NewMolecule->SetActorInitialTemperature(MoleculesInitialTemperature);
> NewMolecule->SetHeatCapacity(MoleculeHeatCapacity);
> ```

You know what? Performances really suck right now, so I decided that "the future" is now. Let's delete this crap. The molecules initial temperature and heat capacity will be set from the BP_AirMolecule class and that's it :sunglasses:.

Unfortunately the scatterer bug is still there :thinking: It's strange that I never noticed it before, but apparently that's the case. I think the only way to solve this is to activate ticking for the molecules only after each one of them has been spawned. This would be another scatterer `BeginPlay()` operation :sweat_smile: However, this one is waaaaay more important than the ones I just deleted.

I could have a function that simply sets `PrimaryActorTick.bCanEverTick = true` to every molecule and is called in `BeginPlay()` after `MoleculesCubicScattering()`. The problem though is that, because of how the `HeatExchange()` function is made, if a non-molecule actor starts the game surrounded by the scatterer and has a greater temperature than the air one, it can already start exchanging heat with the molecules even if they aren't ticking.  
`AActor::AddTickPrerequisiteActor()` could be useful, it's used to make actor tick after another one. From the gamemode, I could make all the `TSubclassOf<AActorTemperatureBase>` (therefore including the air molecules) in the level tick after the scatterer starts ticking. The problem is that right now the scatterer isn't ticking at all, there would be no use for it to tick except for this thing I just wrote. Mmh, there is probablly a better solution to this.

[Here](https://docs.unrealengine.com/en-US/API/Runtime/Engine/GameFramework/AActor/bAllowTickBeforeBeginPlay/index.html) it says that Unreal doesn't normally allow actor to tick before `BeginPlay()` is executed, therefore this isn't my problem: my problem is that different air molecules have their `BeginPlay()` executed at different times becuase I spawn them one by one.

## 11/19/20

`AActor::AddTickPrerequisiteActor()` on tells the actorA "every tick, you should wait for ActorB tick before you tick". So it's not about start ticking, it's about ticking every frame, I completely misinterpreted this: it's about enforcing a ticking order frame by frame.

Turning the molecules heat capacity up makes the bug essentially invisible, because the first air molecules that spawn (the ones in the top right) don't change their temperature a lot before all the others have spawned too

One thing I could try is to first cycle to compute all spawn locations and put them in a TArray and only after that cycle through this array to spawn all the molecules. However, I feel the thing that really slows the process is the spawning itself, not the fact that I have to compute the location first, so...

Temperature components tick can be disabled, in which case obviously `HeatExchange()` is not called (it is called during Tick after all). If on `this` component tick is enabled but during `SearchColdBodiesNearby()` it finds some components on which it is not, no heat exchange occurs. This is not thermostat behavior: for thermostat behavior, one of the two bodies exchange heat but one of them doesn't change temperature because its heat capacity is infinite. I can set that the molecules start with tick disabled from the blueprint! Once the scatterer has spawned all of them, it cycles through all of them and enables the ticking. This should be a very fast cycle, so the bug shouldn't show anymore.

[You can't disable tick for an actor and its components with a single instruction](https://answers.unrealengine.com/questions/895709/how-can-i-disable-an-actor-with-its-components.html). I should disable the ticking for both the temperature component of the molecule (`SetComponentTickEnabled`) and the molecule itself (`SetActorTickEnabled`). The problem is that the component's `StartWithTickEnabled` cannot be set from the blueprint, it's a class default but the BP_AirMolecule blueprint isn't the component blueprint, is the actor blueprint. Shit: I may be forced to make the following changes:
1. Set StartWithTickEnabled false for the UTemperatureComponent
2. In AActorTemperatureBase set the temperature component tick to true at BeginPlay
3. Create a c++ AirMolecule class, where the temperature component isn't set to true at BeginPlay

This is shitty. Isn't there another way?

#### How tick works?

The `AActor` class has a public object called `PrimaryActorTick` of type `FActorTickFunct`. `FActorTickFunct` inherits from [`FTickFunction`](https://docs.unrealengine.com/en-US/API/Runtime/Engine/Engine/FTickFunction/index.html): here we have the `bCanEverTick` and `bStartWithTickEnabled` properties!

Analogously, `UActorComponent` class has a public object called `PrimaryComponentTick` of type `FActorComponentTickFunction` that inherits from `FTickFunction`.

Since both `PrimaryActorTick` and `PrimaryComponentTick` are public, I can change this property from outside. I could realize the plan above. Or maybe there's something else that I can do?

## 11/21/20

I opted for this solution:
```cpp
// AActorTemperatureScatterer::MoleculesCubicScattering() 

// We enable all molecules to exchange heat. This is done in this final cycle because spawn is
// a slow process, a lot of time passes between a molecule spawn and another, and we don't want
// molecules that spawned before others to start exchanging heat until all air is spawned
for(AActorTemperatureBase* AirMolecule : AirMolecules)
  AirMolecule->GetTemperatureComponent()->ToggleHeatExchange();
```
Where
```cpp
// --- TemperatureComponent.h

UPROPERTY(EditAnywhere, Category = "Heat Capacity")
bool bCanExchangeHeat = true;

// --- TemperatureComponent.cpp

void UTemperatureComponent::ToggleHeatExchange() 
{
    bCanExchangeHeat = !bCanExchangeHeat;
}
```
and
```cpp
UTemperatureComponent* AActorTemperatureBase::GetTemperatureComponent() const
{
	return TemperatureComp;
}
```
Of course, `bCanExchangeHeat` must be set to false in BP_AirMolecule for this to work. And... it works, but doesn't solve the bug.

This means that the bug isn't caused by what I think, it's not because some air molecules spawn before others and start exchanging heat earlier, it's something else, and I don't know what.

## 11/22/20

Ok, the problem is that at the beginning of gameplay some temperatures become negative while others explode to infinity, I checked this via editor using the pause button. Now I need to figure out why, but it surely is something that's going on in `HeatExchange()`.

The `DeltaTemperature` value never becomes negative, I checked this with:
```cpp
// Inside HeatExchange()
if (DeltaTemperature < 0.f)
    UE_LOG(LogTemp, Warning, TEXT("Negative DeltaTemperature %f"), DeltaTemperature);
Temperature -= ThermalConductivity * SurfaceArea * DeltaTemperature * DeltaTime / (Distance * 
AverageHeatCapacity);
Other->Temperature += ThermalConductivity * SurfaceArea * DeltaTemperature * DeltaTime / 
(Distance * Other->AverageHeatCapacity);
```
This:
```cpp
// Inside HeatExchange()
if (Temperature > MaxTemperature || Other->Temperature < MinTemperature)
  UE_LOG(LogTemp, Warning, TEXT("HOT %s: %f --- COLD %s: %f"), *(GetOwner()->GetName()), Temperature, *(Other->GetOwner()->GetName()), Other->Temperature);
Temperature -= ThermalConductivity * SurfaceArea * DeltaTemperature * DeltaTime / (Distance * 
AverageHeatCapacity);
Other->Temperature += ThermalConductivity * SurfaceArea * DeltaTemperature * DeltaTime / 
(Distance * Other->AverageHeatCapacity);
```
revealed that the molecules on the right tend to go below the MinTemperature.

This did the trick:
```cpp
// Inside HeatExchange()
UE_LOG(LogTemp, Warning, TEXT("DeltaTime: %f"), DeltaTime);
UE_LOG(LogTemp, Warning, TEXT("HOT %s: %f --- COLD %s: %f"), *(GetOwner()->GetName()), 
Temperature, *(Other->GetOwner()->GetName()), Other->Temperature);
Temperature -= ThermalConductivity * SurfaceArea * DeltaTemperature * DeltaTime / (Distance * 
AverageHeatCapacity);
Other->Temperature += ThermalConductivity * SurfaceArea * DeltaTemperature * DeltaTime / 
(Distance * Other->AverageHeatCapacity);
```
The logs show that this is a DeltaTime problem: this value can oscillate up to 0.4, which is very high and cause the cold body to receive so much heat that it goes beyond the hot one and viceversa. This can cause temperatures to go below 73K and above 2273K potentially. Using a constant scripted value for the DeltaTime would cause the game to run differently on different computers, which is unacceptable!

What is still not clear to me is:
1. Why if I place the man-like actor in the top right corner this doesn't happen
2. The formula (3) theoretically admits sub-zero kelvin temperatures in extreme conditions. Maybe I am missing something?

Theoretically, this problem can be solved in a very simple way: increasing the heat capacity of the molecules to decrease the dT. Lol. Or I can modify the 260.f scripted value.

The worst possible DeltaTime I've seen is 0.4. The highest possible DeltaTemperature is 2200K. Based on this two values, I can compute the min acceptable heat capacity.

## 11/26/20

After a discussion with Paris and Nicole I came to the conclusion that the formula I am using for the temperature increase dT is correct. On one hand this is nice, it means that I am not completely stupid and that my madness has some meaning. However, this also means that I am fucked:  
I can't simply re-scale the thermal conductivity so that k*dt = const like they suggest, because If I do that then the dT increment only depends on the initial temperatures of the cold and hot bodies, which means that the computer that runs `UTemperatureComponent::TickComponent` more times per second is gonna reach thermal equilibrium earlier.

For the moment I found a good balance by using step = 100 (with a huge 30x30 m^2 map the game is still fluid), `SurfaceArea = 100.f`, `ThermalConductivity = 2.6f`. I made various tests and the delta apparently doesn't break(hot body doesn't go below cold one and viceversa). Maybe the only "solution" is just to accept the fact that I must not use values of `Step` and `HeatCapacity` that are too small.

## 11/27/20

Trying to make the game pause/unpause when the spacebar is pressed, like in [DA: Origins](https://dragonage.fandom.com/wiki/Controls_(Origins)); `APlayerController::SetPause()` might be the right function.

The pause works with the `UGameplayStatics` functions to set and get pause status. `.bExecuteWhenPaused = true` is needed to make the spacebar input readable during pause. I tried to `.bExecuteWhenPaused = true` in every action and axis binding so that I can also move the camera during pause, but even though axis bindings update during pause the camera doesn't follow. [This guy](https://answers.unrealengine.com/questions/205732/pause-the-game-but-let-the-player-camera-move.html) had my same problem and solved apparently.

Apparently, my approach is wrong. The type of game I am trying to game is a Real Time With Pause (RTWP) usign queued orders. To better achieve this [here](https://forums.unrealengine.com/development-discussion/blueprint-visual-scripting/96881-rtwp-implementing-real-time-with-active-pause-and-queued-orders-game-mechanic) they suggest to do the pausing by changing the Custom Time Dilation on the actors I want to pause, setting it to zero (Global Time Dilation cannot be set to zero).

I tried to change the GlobalTimeDilation with this code:
```cpp
TimeDilation = FGenericPlatformMath::Abs<float>(TimeDilation - 1.f);
UGameplayStatics::SetGlobalTimeDilation(GetWorld(), TimeDilation);
CustomTimeDilation = 1.f;
```
but unfortunately, this parameter can't be set to zero, I get the following log:
> LogBlueprintUserMessages: Warning: Time Dilation must be between 0.000100 and 20.000000.  Clamped value to that range.

It's a shame becuase the whole process would've been a lot faster this way. Oh well, let's just set the custom time dilation to zero for every actor except the camera instead:
```cpp
// Abs ensures that TimeDilation-1 always assumes 0 or 1 as values
TimeDilation = FGenericPlatformMath::Abs<float>(TimeDilation - 1.f);
// An Actor's DeltaTime is computed as real DeltaTime * CustomTimeDilation.
// CustomTimeDilation = 1 => actor ticks normally
// CustomTimeDilation = 0 => actor stops ticking
for (TActorIterator<AActorTemperatureBase> TIterator(GetWorld()); TIterator; ++TIterator)
	TIterator->CustomTimeDilation = TimeDilation;
```

## 11/28/20

I'll now be working on party members and their selection. I found [a video](https://www.youtube.com/watch?v=nMOganG-YqU&t=48s&ab_channel=JoshClark) that explains how to set up a party system like the ones in old BioWare games. Interaction with NPCs is an argument of the video but I'm not interested in it yet.

The problem is that his approach is more like Mass Effect than DA: Origins, he can't move the camera around freely. But I think I can still learn a lot from the video, so I'm gonna watch it.

### Notes on the video

- Nav mesh to cover all map to that AI can move
- **3rd person character BP**
  - Has AI controller ptr property (there is an AI controller for party members so that they can follow you around)
  - At BeginPlay get AIController and cast it to your AI party class (11:00)
  - Overrided Unpossessed (fired when pawn is no longer possessed by a controller) (12:36)
- **AI controller**
  - BeginPlay: get player controller and set timer by event on a custom event called "MoveToPlayer". This event is fired every X second and simply signals to the AI controlled that the controlled pawn should be moved towards the pawn currently controlled by the player (16:27)
- **Dummy pawn**, i.e. the "camera pawn" in my case. This is spawned at beginplay at playerstart and controlled by our playercontroller. This pawn will then spawn the party of pawns and says to the player controller that it is now able to posses them. This is needed only at startup to handle the exchange.
  - BeginPlay (26:57):
    - Function SpawnPawns: Custom function that spawns an array of pawns that are actually the ones we want to control and then returns them to the player controller. Fired at beginplay. (26:57, the function is some minutes before that.
    - He calls an event dispatcher called `PawnsSpawned` after the function. What's an event dispatcher? The official documentation says: by binding one or more events to an Event Dispatcher, you can cause all of those events to fire once the Event Dispatcher is called. The c++ counterpart of event dispatchers seems to be [delegates](https://docs.unrealengine.com/en-US/Programming/UnrealArchitecture/Delegates/index.html). [Here](https://forums.unrealengine.com/community/community-content-tools-and-tutorials/13915-tutorial-creating-and-using-delegates-c-and-accessing-them-in-blueprints) more on delegates is explained.
- **PlayerController**
  - BeginPlay: Get controlled pawn (that at the beginning is the dummy pawn), cast to dummy pawn obviously, and then bind to the `PawnsSpawned` event dispatcher the `AssumedControl` event. The second event automatically has as input the list of pawns that the DummyPawn spawned! COOL!! We then tell the playercontroller to posses the pawn at the index specified from the editor. We finally unbind all events from PawnsSpawned event dispatcher, we don't care about the dummy pawn anymore. (30:24)
  - Now we want to be able to switch between each of the pawns and have AIControllers take over. We take the input for `SwitchCharacter` (it's an action binding on a key you decide) (32:57)
  - We finally want to be able to run to the npc when pressing the interact key. This is something I'm not really interest into right now, but I think it's very important because it shows you that by changing the controlled character the functionalities are mainteined because they're on the player controller. At the end is also explained how to override AI instructions if you want to take back control over the character

### Back to ScalarField

Ok, so my situation is different because I don't want the player to able to control the characters that are party members directly (Mass Effect style). The player should be able to only control the camera and give instructions to the characters through it. The characters are actually controlled by the AI Controller. I think that the implementation of this mechanism is easier than the one shown in the video.
1. I should spawn the characters and select the first one by default
2. Keep a reference of the selected one so that when the player gives inputs (like move here, attack this) they apply to the selected character.
3. Make the non-selected character follow the selected one outsite battles

The party members must be characters, they can't be pawns becuase only characters can use nav-mesh movement, which is crucial to move the party around avoiding obstacles etc.

Alright, I am gonna try to create an `ACharacterTemperature` c++ class! The hard part is that the root component of a character is a normal capsule component, while I'd like to have a temperature component as root! Let's see if I can work around this... maybe I found [something](https://answers.unrealengine.com/questions/290387/how-to-change-root-componet-of-acharacter.html).

## 11/29/20

Added UE4 mannequin stuff by creating a dummy 3rd person BP project and migrating the mannequin folder from there.

Since characters have an inherited skeletal mesh by default and I can't remove it and use a custom skeletal mesh, I need to change the way heat capacity is stored. I decided to simplify the whole process: the heat capacity will simply be a temperature component attribute and the heat capacity mesh component will be deleted.

## 12/01/20

After many refactorings I finally started working on the party member selection. [Here](https://answers.unrealengine.com/questions/126951/how-to-reference-blueprint-as-a-type-in-c.html) it is explained how to use BP classes in C++.

## 12/02/20

The "dummy pawn" mentioned in [the video](https://www.youtube.com/watch?v=nMOganG-YqU&t=48s&ab_channel=JoshClark) at 17:00 is our `PawnPlayerCamera`; we're going to spawn the party members via this.

I am blocked at minute 21:00 because of many errors I am getting. One of them is probablly due to the fact that I'm stupid and I'm trying to have an array of `APartyMember` instances as member of my `PawnPlayerCamera` class, which is ridiculous since `APartyMember` is a child of `PawnPlayerCamera` hahaha, he uses an array of pawn in the video as return type of `SpawnParty`!

## 12/03/20

Spent the whole evening fixing compilation bugs, mostly "undeclared identifier" due to typos hahahahah, I AM SO BAD PLEASE KILL ME NOW. Oh, apparently what I said the previous day isn't true, I can have an `APartyMember` array in the `PawnPlayerCamera` class

## 12/05/20

I managed to make the party members spawn, but they behave in a weird manner... only the first one is visible for some reason, and the meshes are in a position that's completely different from the spawn point I set.  
The problem was that, even if I correctly detached from root the capsule in the `ACharacterTemperature` constructor, I didn't setup the attachment between it and the `UTemperatureComponent`. Now everything works correctly.

Today I almost managed to make the selected character to move on right click! The first time everything goes right, but from the second time onward it starts to bug...

## 12/06/20

The bug is due to the fact that the capsule component isn't my root component at the moment, which probablly breaks the `MoveTo` functions. I just decided to stop going against the engine and attach the temperature component to the capsule component.

Ok, merged, uff. Now, let's make the camera follow the selected party member

## 12/09/20

Looking at a tutorial on how to make a [skill tree system](https://www.youtube.com/watch?v=NZHWbl8IB1Y&ab_channel=Nitrogen)

[Cool website](https://benui.ca/unreal/ui-bindwidget/) of an UI programmer that uses Widgets with C++

## 12/10/20

Applying the skill tree tutorial to my case isn't trivial, not because I have BPs and I only want to use C++, but because my skill tree has to change its content when the selected party member changes, which isn't trivial at all.

The following
```cpp
InputMode.SetWidgetToFocus(SkillTreeWidget);
```
was giving me problems, complaining that the function doesn't take that object but rather a `TSharedPtr<SWidget>`. This can be fixed with the `TakeWidget()` function as explained [here](https://answers.unrealengine.com/questions/357232/how-do-you-convert-a-uuserwidget-to-a-swidget-need.html)
```cpp
InputMode.SetWidgetToFocus(SkillTreeWidget->TakeWidget());
```

TODO: 
1. Play around with the widget opening / closing to see if something breaks while giving input or something like that
2. Choose what should be possible while the widget is opened and what shouldn't.
3. Choose if the game should go in tactical pause while the widget is open
4. Find a way to change the widget content when the selected party member changes, you probablly have to implement a function in the widget tree or something like that.

Video minute 11:32

## 12/12/20

I found a way to change the widget content when the selected party member is changed: I simply moved all the code from `NativeConstruct() ` to `PlayOpenCloseAnimation()`. The widget exists even when it's not visible, it's always in the viewport.

At the moment `SelectedPartyMember` is a member of the skill tree widget, but I'm not completely sure this is needed, I could just pass it as parameter to `PlayOpenCloseAnimation()`. I'll decide if it has to be removed in the future, for the moment I can keep it.

When the skill tree widget is opening I can't use `FInputModeUIOnly` because then I can't close the UI via keys and I'm forced to do so via the UI itself (e.g. by implementing a button).

Next step: apply tactical pause when the widget is called (spacebar is disabled in such case).

## 12/13/20

Forced tactical pause set when menu widgets are opened. It's not possible to change selected party member and order movement when menu widgets are opened.

Okay, now let's keep going with the video! Next step: make the skill slot widget. I followed [this tutorial](https://www.youtube.com/watch?v=UGV-PNyFj_U&feature=emb_logo&ab_channel=Pixovert) to make a frame image to use in the skill slot widget with Gimp because I'm a fucking noob at making assets. 

At 15:27 of the video he uses the [Make SlateBrush node](https://docs.unrealengine.com/en-US/BlueprintAPI/Utilities/Struct/MakeSlateBrush/index.html), whose c++ correspective is [this constructor](https://docs.unrealengine.com/en-US/API/Runtime/SlateCore/Styling/FSlateBrush/__ctor/2/index.html) of the `FSlateBrush` class.  
The problem is that this constructor is protected, so I'll have to use one of the derived classes, an `FSlateImageBrush` seems ideal.

Make a `UObject*` that has expose on spawn as attribute. Then, on `USkillSlot::NativeConstruct()`:
1. Create an `FSlateImageBrush` and set its image as the value of the UObject ptr that has expose on spawn
2. Use `UImage::SetBrush()` to set this new brush as the image brush. This should be enough to draw the image.

I had to add `"SlateCore"` module to `PublicDependencyModuleNames.AddRange()` in `ScalarField.Build.cs` (located in Source/ScalarField) to make linker errors due to `FSlateColor` go away (it's a parameter of the `FSlateImageBrush` constructor).

I am at 17:48 and I'd like to find a way to do binding entirely within c++ (i.e., an automatic update of the widget content everytime something happens). This is needed because the textbox with the skill name and description should change each time the variables SelectedSkillName and SelectedSkillDescr in the widget tree update (which occurs at mouse hover).

[Source 1](https://forums.unrealengine.com/development-discussion/c-gameplay-programming/1433168-binding-a-function-to-a-widget-button)
[Source 2](https://forums.unrealengine.com/development-discussion/c-gameplay-programming/1532030-how-property-binding-is-working)

I just notice that `UImage` class has delegate members :eyes:. [Here](https://forums.unrealengine.com/community/community-content-tools-and-tutorials/13915-tutorial-creating-and-using-delegates-c-and-accessing-them-in-blueprints) is a delegates tutorial in c++.

## 12/14/20

Maybe delegates aren't needed... let's experiment a bit:

[Getting canvas from widget](https://answers.unrealengine.com/questions/145515/how-can-i-get-the-position-of-my-widget-on-the-par.html)

A lot of progress today! I am at 23:11 in the video!

Old array content:
```cpp
Array<FPartyMemberParams> MembersToSpawn;
	MembersToSpawn.Emplace(FPartyMemberParams{
		TEXT("Diane"),
		FVector(100.f, 0.f, 0.f),
		15
	});
	MembersToSpawn.Emplace(FPartyMemberParams{
		TEXT("Seth"),
		FVector(0.f, -100.f, 0.f),
		5
	});
	MembersToSpawn.Emplace(FPartyMemberParams{
		TEXT("Peter"),
		FVector(0.f, 100.f, 0.f),
		1
	});
```

Rebuilding part of my structure so that I have a widget instance for every party member (needed to keep track of the acquired skill points for every member and to have different skill trees) and that the party can be se through editor (necessary to set the widget class for each member).

Problem: 
1. How to get the owner character instance (GetPlayerPawn->CycleThroughPartyMembers->Set the one that has the ptr to the widget = this)
2. How to make the TArray<FPartyMemberParams> show in the editor (size error)

## 12/16/20

Ook, this made me crazy:
> 2. How to make the TArray<FPartyMemberParams> show in the editor (size error)

I was getting an unreadable error because of this code:
```cpp
// Forward declaration of FPartyMemberParams
struct FPartyMemberParams;

//...
// ### inside class APawnPlayerCamera ###
UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Party Members", meta=(AllowPrivateAccess=true))
TArray<FPartyMemberParams> MembersToSpawn; // Here the struct is being used by value
```
My mistake was that, as explained [here](https://answers.unrealengine.com/questions/441067/making-an-array-of-structs-a-uproperty.html), **you can't forward declare a type used by value, only by pointer or by reference**.  
Now you might think:"Well, just make a `TArray<FPartyMemberParams*>` then! Nope, you can't: as explained at the same link, USTRUCT types can't be pointers when used with a UPROPERTY.

The solution? I just included the header file where the USTRUCT is defined, PartyMember.h, in the header file where I have the array, PlayerCamera.h, and removed the forward declaration.

24:40

## 12/18/20

I am having a problem: the widget seems to be overlapping. For the moment I'll ignore this and try to make the skill slots to appear / disappear with the widget, then I'll come back to it.

We need to make a timeline, which is not an easy task in c++ (minute 29:00 of the video)

[This](https://answers.unrealengine.com/questions/115830/why-is-timeline-bindufunction-only-called-once.html) and [this](https://answers.unrealengine.com/questions/384597/link-to-timelines-doc-for-c.html#answer-400083) are important to realize a timeline in c++, the second one in particular seems to allow me to define the curve and use it directly inside the skill slot instead of inside the party member.

I simply can't have multiple widgets on the viewport and expect all of them to work(i.e. not to overlap). I will:
1. Create two child classes of `APartyMember`: `APartyMage` and `APartyWarrior`. Each of them has a certain skill tree class and a corresponding array of skills. 
2. This way I can add to / remove from viewport the skill tree each time I change character, but the data is safe thanks to the array I keep in each character.

For the moment I'll avoid the two children classes of `APartyMember`, just to see if my idea works. If it does, then I just have to create the 2 subclasses, use `TSubclassOf<APartyMember>` instead of  `TSubclassOf<USkillTreeWidget>` in `FPartyMemberParams` (`TSubclassOf<USkillTreeWidget>` will be a member of `TSubclassOf<APartyMember>`)

## 12/20/20

Progress!! :muscle: Now data about skills is stored in the party members instead of the widgets themselves. Thanks to this, now I don't lose track of which skills have been acquired and which have not when I change the selected party member :) I made some party member subclasses (mage, warrior), all instances of the same class have the same skills.

What is left to do:
1. Some skills should require others to be unlocked, see final part of [the video](https://www.youtube.com/watch?v=NZHWbl8IB1Y&ab_channel=Nitrogen)
2. At the moment, I have to manually add the fade animation to each skill slots I add to my skill tree widgets. I should find a way to automatically add it to every skill slots when the skill tree widget is created.

The first step has been tackled successfully

## 12/21/20

For step 2 I followed [this](https://answers.unrealengine.com/questions/384597/link-to-timelines-doc-for-c.html#answer-400083) and the [skill tree video](https://www.youtube.com/watch?v=NZHWbl8IB1Y&t=1105s&ab_channel=Nitrogen) around 30:00.  
It took me a lot to make it work because, for many hours, I tried to define the `UTimelineComponent` in the `USkillSlot` class, which is wrong because it should go in `APawnPlayerCamera` class since this latter is an actor and can accept components (`AddInterpFloat` somehow binds the timeline component to the tick function).

Differently from the video I didn't use delegates because it seemed a bit of a stretch to me, but I could have since I found [an amazing explanation of delegates](https://unreal.gg-labs.com/wiki-archives/macros-and-data-types/delegates-in-ue4-raw-c++-and-bp-exposed) + I had [this old link](https://forums.unrealengine.com/community/community-content-tools-and-tutorials/13915-tutorial-creating-and-using-delegates-c-and-accessing-them-in-blueprints).

Basically, when you have a delegate:
1. You declare it above the class that will fire it FirstClass.h
2. Create an instance of such delegate inside the class
3. Use the `Broadcast()` method somewhere in the FirstClass.cpp to tell the `UFUNCTION`s in other classes that are listening that they should start with the values you pass to them
4. Define such `UFUNCTION`s in the OtherClasses.h
5. Use `AddDynamic()` or `AddUObject()` (depending of the type of delegate you declared above the first class) to say to these functions that they should listen to the delegate `Broadcast()`

## 12/22/2020

Bug to fix: when I start the game the skillslots are hooverable and clickable even though the skill tree isn't visible. This doesn't happen anymore after the widget is opened and then closed, but happens again if a change selected character. In other words: is something that happens at `NativeConstruct` (which is called from `AddToViewport) or `ChangeSkillTree`

I fixed this by using `SetIsEnabled` to true only when the widget is opened.