# ScalarField

By definition, a _Scalar Field_ is a funcion that associates a number to every point in space.

I want to recreate this concept in Unreal Engine 4: given a level, I want to map each point to a number. There are probablly many ways to do so, I am going to use this repo to experiment some ideas I have :alembic:
