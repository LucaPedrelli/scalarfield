// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/CapsuleComponent.h"
#include "TemperatureComponent.generated.h"

/**
 * 
 */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SCALARFIELD_API UTemperatureComponent : public UCapsuleComponent
{
	GENERATED_BODY()
	
public:
	// Sets default values for this component's properties
	UTemperatureComponent();

	virtual void BeginPlay() override;

	// Clamps the InitialTemperature when it is changed via editor
	#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	#endif

	// Called every frame
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

	float GetTemperature() const;

	// Changes InitialTemperature value, clamps it, and sets Temperature = InitialTemperature
	void SetInitialTemperature(double InitTemperature);

	static const double MaxTemperature;
	static const double MinTemperature;

private:
	// ##### ATTRIBUTES #####
	// Measurment Units: K (Kelvin degrees)
	UPROPERTY(EditAnywhere, Category = "Temperature", meta=(AllowPrivateAccess=true))
	double InitialTemperature = 298.;

	// Measurment Units: K (Kelvin degrees)
	UPROPERTY(VisibleAnywhere, Category = "Temperature", meta=(AllowPrivateAccess=true))
	double Temperature = InitialTemperature;

	// Measurment Units: J / K
	UPROPERTY(EditAnywhere, Category="Heat Capacity", meta=(AllowPrivateAccess=true))
	float HeatCapacity = 4120.f; // 1 kg of water as default

	UMaterialInstanceDynamic* MaterialInstance;

	// ##### FUNCTIONS #####
	// SearchColdBodiesNearby searches for overlapping UTemperatureComponents whose Temperature is less than this->Temperature
	// For each UTemperatureComponent found, HeatTransmision() gets called. 
	void SearchColdBodiesNearby(float DeltaTime);

	// HeatTransmission evaluates the temperature variation of this component, that gets colder, and the other, that gets
	// hotter, in the time DeltaTime. Then, the temperatures are varied accordingly.
	void HeatTransmission(UTemperatureComponent* Other, float DeltaTime);

	FLinearColor ComputeColorWithTemperature() const;
};
