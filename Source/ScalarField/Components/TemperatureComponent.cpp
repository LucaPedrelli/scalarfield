// Fill out your copyright notice in the Description page of Project Settings.


#include "TemperatureComponent.h"
#include "ScalarField/Actors/ActorTemperatureBase.h"
#include "ScalarField/Characters/CharacterTemperature.h"

// Temperatures are in Kelvin degrees
const double UTemperatureComponent::MaxTemperature = 2273.; // 2000 Celsius degrees, high enough to melt most metals
const double UTemperatureComponent::MinTemperature = 73.; // -200 Celsuis degrees, low enough to condensate Nitrogen

UTemperatureComponent::UTemperatureComponent() 
{
    // Set this component to be initialized when the game starts, and to be ticked every frame. You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

    // ECollisionChannel::ECC_GameTraceChannel1 is the enum value my custom Heat channel has been assigned to
    // TemperatureComponent is in the Heat channel
    SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);
    // TemperatureComponent ignores all channels except the Heat channel, so that it can interact only with other
    // components of the same kind.
    SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
    SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Overlap);

}

void UTemperatureComponent::BeginPlay() 
{
    Super::BeginPlay();
    if (AActorTemperatureBase* OwnerActor = Cast<AActorTemperatureBase>(GetOwner()))
        MaterialInstance = OwnerActor->GetMesh()->CreateDynamicMaterialInstance(0, OwnerActor->GetMesh()->GetMaterial(0), TEXT(""));
    else if (ACharacterTemperature* OwnerCharacter = Cast<ACharacterTemperature>(GetOwner()))
        MaterialInstance = OwnerCharacter->GetMesh()->CreateDynamicMaterialInstance(0, OwnerCharacter->GetMesh()->GetMaterial(0), TEXT(""));
}

#if WITH_EDITOR
void UTemperatureComponent::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) 
{
    FProperty* Property = PropertyChangedEvent.Property;
    FName PropertyName = (Property != NULL) ? Property->GetFName() : NAME_None;
	if (PropertyName == GET_MEMBER_NAME_CHECKED(UTemperatureComponent, InitialTemperature))
    {
         if (FDoubleProperty* InitTempProperty = CastFieldChecked<FDoubleProperty>(Property))
		    SetInitialTemperature(
                InitTempProperty->GetFloatingPointPropertyValue(Property->ContainerPtrToValuePtr<float>(this))
            );
    } else if (PropertyName == GET_MEMBER_NAME_CHECKED(UTemperatureComponent, HeatCapacity))
    {
        if (FFloatProperty* HeatCapProperty = CastFieldChecked<FFloatProperty>(Property))
        {
		    float TemporaryHeatCapacity = HeatCapProperty->GetFloatingPointPropertyValue(Property->ContainerPtrToValuePtr<float>(this));
            HeatCapacity = TemporaryHeatCapacity > 1.f ? TemporaryHeatCapacity : 1.f;
        }
    }
    Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif

void UTemperatureComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) 
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    SearchColdBodiesNearby(DeltaTime);

    if (MaterialInstance)
        MaterialInstance->SetVectorParameterValue(TEXT("TemperatureColor"), ComputeColorWithTemperature());
}

void UTemperatureComponent::SearchColdBodiesNearby(float DeltaTime) 
{
    TArray<UPrimitiveComponent*> OverlappedComps;
    GetOverlappingComponents(OverlappedComps);
    for (UPrimitiveComponent* OverlappedComp : OverlappedComps)
        // This cast isn't strictly necessary considering how I configured heat channel collisions, but you never know...
        if (UTemperatureComponent* OtherTemperatureComp = Cast<UTemperatureComponent>(OverlappedComp))
            if (Temperature > OtherTemperatureComp->Temperature)
                HeatTransmission(OtherTemperatureComp, DeltaTime);

}

void UTemperatureComponent::HeatTransmission(UTemperatureComponent* Other, float DeltaTime) 
{
    // Explanation of this formula in Heat_transmission_formula.pdf
    // Approximations:
    // - Distance between bodies fixed to 1cm, valid because temperature components' dimensions are
    // often very close to the actor's mesh
    // - Thermal conductivity 2.6 W/(cm * K) (Air)
    float Distance = 1.f;
    float DeltaTemperature = Temperature - Other->Temperature;
    float ThermalConductivity = 2.6f;
    // TODO: find a way to give a better estimate of SurfaceArea
    float SurfaceArea = 100.f; // 100cm^2
    // UE_LOG(LogTemp, Warning, TEXT("DeltaTime: %f"), DeltaTime);
    // UE_LOG(LogTemp, Warning, TEXT("HOT %s: %f --- COLD %s: %f"), *(GetOwner()->GetName()), Temperature, *(Other->GetOwner()->GetName()), Other->Temperature);
    Temperature -= ThermalConductivity * SurfaceArea * DeltaTemperature * DeltaTime / (Distance * HeatCapacity);
    Other->Temperature += ThermalConductivity * SurfaceArea * DeltaTemperature * DeltaTime / (Distance * Other->HeatCapacity);
}

float UTemperatureComponent::GetTemperature() const
{
    return Temperature;
}

void UTemperatureComponent::SetInitialTemperature(double InitTemperature) 
{
    InitialTemperature = FMath::Clamp<double>(InitTemperature, UTemperatureComponent::MinTemperature, UTemperatureComponent::MaxTemperature);
    Temperature = InitialTemperature;
}

FLinearColor UTemperatureComponent::ComputeColorWithTemperature() const
{
	float ColorScalePos = (Temperature - UTemperatureComponent::MinTemperature) / (UTemperatureComponent::MaxTemperature - UTemperatureComponent::MinTemperature);
	// Blue: 	(0.f, 0.f, 1.f) 	at 73K   (-200°C)	-> 0.f
	// Cyan: 	(0.f, 1.f, 1.f) 	at 283K  (10°C)		-> (283-73)/(2273-73)	= 0.095
	// Yellow: 	(1.f, 1.f, 0.f) 	at 303K  (30°C)		-> (303-73)/(2273-73)	= 0.105
	// Orange:  (1.f, 0.27f, 0.f)   at 323K  (50°C)		-> (323-73)/(2273-73)	= 0.114
	// Red: 	(1.f, 0.f, 0.f) 	at 473K  (200°C)	-> (473-73)/(2273-73)	= 0.182
	// WineRed: (0.82f, 0.f, 0.41f) at 1273k (1000°C)	-> (1273-73)/(2273-73) 	= 0.545
	// For greter temperatures, we keep WineRed

	if (ColorScalePos <= 0.095f) // From blue to cyan
		return FLinearColor(
			0.f,
			ColorScalePos / 0.095f, 
			1.f, 
			1.f
		);
	else if (ColorScalePos <= 0.105f) // From cyan to yellow
		return FLinearColor(
			(ColorScalePos - 0.095f) / (0.105f - 0.095f), 
			1.f, 
			1.f - (ColorScalePos - 0.095f) / (0.105f - 0.095f), 
			1.f
		);
	else if (ColorScalePos <= 0.114f) // From yellow to orange
		return FLinearColor(
			1.f, 
			1.f - (1.f - 0.27f) * (ColorScalePos - 0.105f) / (0.114f - 0.105f), 
			0.f, 
			1.f
		);
	else if (ColorScalePos <= 0.182f) // From orange to red
		return FLinearColor(
			1.f, 
			0.27f * (1 - (ColorScalePos - 0.114f) / (0.182f - 0.114f)), 
			0.f, 
			1.f
		);
	else if (ColorScalePos <= 0.545f) // From red to wine red
		return FLinearColor(
			1.f - (1.f - 0.82f) * (ColorScalePos - 0.182f) / (0.545f - 0.182f), 
			0.f, 
			0.41f * (ColorScalePos - 0.182f) / (0.545f - 0.182f), 
			1.f
		);
	else // For temperatures greater that
		return FLinearColor(0.82f, 0.f, 0.41f);
}
