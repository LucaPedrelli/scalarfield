// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "ScalarField/Characters/PartyMember.h"
#include "Components/TimelineComponent.h"
#include "PawnPlayerCamera.generated.h"

class APartyMember;
class APlayerCameraController;
class UCameraComponent;
class USkillTreeWidget;
class USpringArmComponent;

UCLASS()
class SCALARFIELD_API APawnPlayerCamera : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APawnPlayerCamera();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	static APartyMember* GetSelectedPartyMember();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// ##### COMPONENTS ######
	// Our root component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess=true))
	USceneComponent* Scene;

	// Arm between the scene component and the actual camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess=true))
	class USpringArmComponent* SpringArm;

	// Camera through which the player sees the world
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess=true))
	class UCameraComponent* Camera;

	// ##### FUNCTIONS #####
	void SpawnParty();
	void ChangeSelectedMember(bool bMoveCamera);
	void ChangeSkillTree();
	void ApplyTimeDilationToWorld(float TimeDilationToApply);

	// Function called when the timeline is executed with the current value of the float curve
	UFUNCTION()
	void SetSkillSlotsOpacity(float CurveValue);

	// ### AXIS BINDINGS ###
	void XTranslation(float AxisValue);
	void YTranslation(float AxisValue);
	void YawRotation(float AxisValue);
	void Zoom(float AxisValue);

	// ### ACTION BINDINGS ###
	void ToggleAirVisibility();
	void ToggleTacticalPause();
	void MoveSelectedMemberToCursor();
	void SelectByLeftClick();
	void SelectByDoubleLeftClick();
	void UnlockCameraFromSelectedMember();
	void ToggleSkillTreeVisibility();

	// ##### ATTRIBUTES #####
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Camera Movement", meta=(AllowPrivateAccess=true))
	float TranslationSpeed = 25.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Camera Movement", meta=(AllowPrivateAccess=true))
	float RotationSpeed = 25.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Camera Movement", meta=(AllowPrivateAccess=true))
	float ZoomSpeed = 50.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Camera Movement", meta=(AllowPrivateAccess=true))
	float MaxZoom = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Camera Movement", meta=(AllowPrivateAccess=true))
	float MinZoom = 1000.f;

	// When 1.f, everything is normal; when 0.f, some actors cannot tick (see ApplyTimeDilationToWorld).
	// "Outside menus" because, regardless of this value, when some widgets are opened we force
	// tactical pause to be on.
	float DilationOutsideMenus;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Party Members", meta=(AllowPrivateAccess=true))
	TArray<FPartyMemberParams> MembersToSpawn;

	TArray<APartyMember*> PartyMembers;

	static APartyMember* SelectedPartyMember;

	bool bIsCameraLocked;

	APlayerCameraController* CameraController;

	USkillTreeWidget* SkillTree;

	bool bIsSkillTreeVisible;

	// ### Skillslots fade animation stuff ###
	UPROPERTY()
	UTimelineComponent* FadeTimeline;
	UPROPERTY()
	UCurveFloat* fCurve;
	FOnTimelineFloat InterpFunction{}; // Delegate

	// ### AXIS BINDINGS ###
	FVector2D CameraMovementInput;
	FVector2D CameraRotationInput;
	float ZoomInput;
};
