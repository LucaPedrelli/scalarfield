// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnPlayerCamera.h"
#include "Camera/CameraComponent.h"
#include "EngineUtils.h"
#include "GameFramework/SpringArmComponent.h"
#include "ScalarField/Actors/ActorTemperatureBase.h"
#include "ScalarField/Actors/ActorTemperatureScatterer.h"
#include "ScalarField/Characters/PartyMember.h"
#include "ScalarField/Controllers/PartyMemberAIController.h"
#include "ScalarField/Controllers/PlayerCameraController.h"
#include "ScalarField/UI/SkillSlot.h"
#include "ScalarField/UI/SkillTreeWidget.h"

APartyMember* APawnPlayerCamera::SelectedPartyMember;

// Sets default values
APawnPlayerCamera::APawnPlayerCamera()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = Scene;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(Scene);
	SpringArm->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	SpringArm->TargetArmLength = 600.f;
	SpringArm->bDoCollisionTest = false;
	SpringArm->bEnableCameraLag = true;
	SpringArm->CameraLagSpeed = 3.0f;
	
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm);

	// Setting up timeline component needed for skillslots fade animation
	static ConstructorHelpers::FObjectFinder<UCurveFloat> Curvy(TEXT("CurveFloat'/Game/Blueprints/UI/BP_FadeCurve.BP_FadeCurve'"));
	if (Curvy.Object) {
		fCurve = Curvy.Object;
	}
	FadeTimeline = CreateDefaultSubobject<UTimelineComponent>(TEXT("TimelineFade"));
	InterpFunction.BindUFunction(this, FName{ TEXT("SetSkillSlotsOpacity") });
}

// Called when the game starts or when spawned
void APawnPlayerCamera::BeginPlay()
{
	Super::BeginPlay();
	// The game starts by allowing TemperatureComponents to tick
	DilationOutsideMenus = 1.f;

	CameraController = Cast<APlayerCameraController>(GetController());

	// By default the skill tree widget is closed
	bIsSkillTreeVisible = false;

	SpawnParty();
	bIsCameraLocked = true;

	// Setting the curve and the function to call for this timeline component
	FadeTimeline->AddInterpFloat(fCurve, InterpFunction, FName{ TEXT("Floaty") });
	FadeTimeline->SetTimelineLength(0.5f);
}

// Called every frame
void APawnPlayerCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Movement is not accepted when the skill tree widget is opened
	if (bIsSkillTreeVisible)
		return;

	// Apply zoom (resize of SpringArm's lenght)
	if(SpringArm)
		SpringArm->TargetArmLength = FMath::Clamp(SpringArm->TargetArmLength + ZoomInput, MaxZoom, MinZoom);

	// Apply a yaw rotation (rotation around the z-axis)
	FRotator NewRotation = GetActorRotation();
	NewRotation.Yaw += CameraRotationInput.X;
	SetActorRotation(NewRotation);

	// Apply translation
	// Translation is determined automatically by the selected party member if the camera is
	// locked on it, otherwise it's determined by the W, S, LeftArrow, and RightArrow keys.
	if (bIsCameraLocked)
	{
		SetActorLocation(SelectedPartyMember->GetActorLocation());
		return;
	}

	if (!CameraMovementInput.IsZero())
	{
		FVector NewLocation = GetActorLocation();
		// Using GetActorForwardVector and GetActorRightVector allows us to move relative to the direction the actor is facing. 
		// Since the camera faces the same way as the actor, this ensures that our forward key is always forward relative to what the player sees.
		NewLocation += GetActorForwardVector() * CameraMovementInput.X;
		NewLocation += GetActorRightVector() * CameraMovementInput.Y;
		SetActorLocation(NewLocation);
	}
}

// Called to bind functionality to input
void APawnPlayerCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// When W or S are pressed, XTranslation() is called. W has scale 1, S has -1
	InputComponent->BindAxis("XTranslation", this, &APawnPlayerCamera::XTranslation);
	// When Left or Right are pressed, YTranslation() is called. Left has scale -1, Right has +1
	InputComponent->BindAxis("YTranslation", this, &APawnPlayerCamera::YTranslation);

	// When A or D are pressed, YawRotation() is called. A has scale -1, D has scale +1
	InputComponent->BindAxis("YawRotation", this, &APawnPlayerCamera::YawRotation);

	// When the mouse wheel axis is being moved, Zoom() is called
	InputComponent->BindAxis("Zoom", this, &APawnPlayerCamera::Zoom);

	// When the Tab key is released, ToggleAirVisibility() is called, which makes the air molecules
	// appear/disappear.
	InputComponent->BindAction("AirVisibility", IE_Released, this, &APawnPlayerCamera::ToggleAirVisibility);

	// TacticalPause is a fake pause achieved by setting the time dilation to zero for each actor
	// except this one. This way, during tactical pause the player can still move the camera
	// around and queue orders for party memebers to be executed without worrying about real time
	// interactions with the enemies and the map.
	InputComponent->BindAction("TacticalPause", IE_Pressed, this, &APawnPlayerCamera::ToggleTacticalPause);

	InputComponent->BindAction("MoveToCursor", IE_Pressed, this, &APawnPlayerCamera::MoveSelectedMemberToCursor);

	InputComponent->BindAction("SelectByLeftClick", IE_Pressed, this, &APawnPlayerCamera::SelectByLeftClick);

	InputComponent->BindAction("SelectByLeftClick", IE_DoubleClick, this, &APawnPlayerCamera::SelectByDoubleLeftClick);

	InputComponent->BindAction("UnlockCameraByTranslation", IE_Pressed, this, &APawnPlayerCamera::UnlockCameraFromSelectedMember);

	InputComponent->BindAction("OpenCloseSkillTree", IE_Pressed, this, &APawnPlayerCamera::ToggleSkillTreeVisibility);
}

void APawnPlayerCamera::XTranslation(float AxisValue) 
{
	CameraMovementInput.X = AxisValue * TranslationSpeed;
}

void APawnPlayerCamera::YTranslation(float AxisValue) 
{
	CameraMovementInput.Y = AxisValue * TranslationSpeed;
}

void APawnPlayerCamera::YawRotation(float AxisValue) 
{
	CameraRotationInput.X = AxisValue * RotationSpeed;
}

void APawnPlayerCamera::Zoom(float AxisValue) 
{
	ZoomInput = AxisValue * ZoomSpeed;
}

void APawnPlayerCamera::ToggleAirVisibility()
{
	for (TActorIterator<AActorTemperatureScatterer> TIterator(GetWorld()); TIterator; ++TIterator)
		TIterator->ToggleMoleculeVisibility();
}

void APawnPlayerCamera::ToggleTacticalPause() 
{
	// Tactical pause is forced when menu widgets are opened, we don't allow changing the state
	if (bIsSkillTreeVisible)
		return;

	// Abs ensures that DilationOutsideMenus-1 always assumes 0 or 1 as values
	DilationOutsideMenus = FGenericPlatformMath::Abs<float>(DilationOutsideMenus - 1.f);
	APawnPlayerCamera::ApplyTimeDilationToWorld(DilationOutsideMenus);
}

void APawnPlayerCamera::ApplyTimeDilationToWorld(float TimeDilationToApply) 
{
	// An Actor's DeltaTime is computed as real DeltaTime * CustomTimeDilation.
	// CustomTimeDilation = 1 => actor ticks normally
	// CustomTimeDilation = 0 => actor stops ticking
	for (TActorIterator<AActorTemperatureBase> TIterator(GetWorld()); TIterator; ++TIterator)
		TIterator->CustomTimeDilation = TimeDilationToApply;
	
	for (TActorIterator<ACharacterTemperature> TIterator(GetWorld()); TIterator; ++TIterator)
		TIterator->CustomTimeDilation = TimeDilationToApply;
}

APartyMember* APawnPlayerCamera::GetSelectedPartyMember()
{
	return SelectedPartyMember;
}

void APawnPlayerCamera::MoveSelectedMemberToCursor() 
{
	// We don't allow moving selected character by right click when menu widgets are opened
	if (bIsSkillTreeVisible)
		return;

	// Get the cursor location
	FHitResult HitResult;
	CameraController->GetHitResultUnderCursorByChannel(UEngineTypes::ConvertToTraceType(ECC_Visibility), true, HitResult);
	FVector CursorLocation = HitResult.Location;

	// Pass it to the correct AIController to MoveToLocation
	if (APartyMemberAIController* SelectedAI = Cast<APartyMemberAIController>(SelectedPartyMember->GetController()))
		SelectedAI->MoveToLocation(CursorLocation);
}

void APawnPlayerCamera::SpawnParty() 
{
	UWorld* CurrentLevel = GetWorld();
	if (!CurrentLevel)
		return;

	FVector CameraPawnLocation = GetActorLocation();

	APartyMember* NewPartyMember;
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = 
		ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	for (const FPartyMemberParams& MemberToSpawn : MembersToSpawn)
	{
		NewPartyMember = CurrentLevel->SpawnActor<APartyMember>(
			MemberToSpawn.PartyMemberClass,
			CameraPawnLocation + MemberToSpawn.SpawnOffset,
			FRotator::ZeroRotator,
			SpawnParams
		);
		if (!NewPartyMember)
			continue;
		NewPartyMember->SetName(MemberToSpawn.CharacterName);
		NewPartyMember->SetSkillpointsAvailable(MemberToSpawn.StartingSkillpoints);
		PartyMembers.Emplace(NewPartyMember);
	}
	// By default the selected party member is the first of the array
	SelectedPartyMember = PartyMembers[0];
	ChangeSkillTree();
}

void APawnPlayerCamera::SelectByLeftClick() 
{
	// Changing character by left click is not allowed when menus are opened
	// TODO: implement buttons to do so when the menu is opened
	if (bIsSkillTreeVisible)
		return;

	ChangeSelectedMember(false);
}

void APawnPlayerCamera::SelectByDoubleLeftClick() 
{
	// Changing character by double left click is not allowed when menus are opened
	// TODO: implement buttons to do so when the menu is opened
	if (bIsSkillTreeVisible)
		return;

	ChangeSelectedMember(true);
}

void APawnPlayerCamera::UnlockCameraFromSelectedMember() 
{
	bIsCameraLocked = false;
}

void APawnPlayerCamera::ChangeSelectedMember(bool bMoveCamera) 
{
	// Get the cursor location
	FHitResult HitResult;
	CameraController->GetHitResultUnderCursorByChannel(UEngineTypes::ConvertToTraceType(ECC_Pawn), true, HitResult);
	// Change the selected party member to the one under the cursor
	if (APartyMember* HitPartyMember = Cast<APartyMember>(HitResult.GetActor()))
		for (APartyMember* PartyMember : PartyMembers)
			if (PartyMember == HitPartyMember)
			{
				SelectedPartyMember = PartyMember;
				ChangeSkillTree();
				// If requested, we reposition the player camera on the selected party member
				if (bMoveCamera)
				{
					SetActorLocation(SelectedPartyMember->GetActorLocation());
					bIsCameraLocked = true;
				} 
				else 
					bIsCameraLocked = false;
			}
}

void APawnPlayerCamera::ChangeSkillTree() 
{
	if (SkillTree)
		SkillTree->RemoveFromViewport();
	
	SkillTree = Cast<USkillTreeWidget>(CreateWidget(CameraController, SelectedPartyMember->GetSkillTreeClass()));
	if (!SkillTree)
		return;
	SkillTree->AddToViewport();
	SkillTree->SetOwnerPartyMember(SelectedPartyMember);
}

void APawnPlayerCamera::ToggleSkillTreeVisibility() 
{
	// If we're not in tactical pause when the skill tree widget is being opened
	if (DilationOutsideMenus == 1.f) 
	{
		if (!bIsSkillTreeVisible) // we're opening the widget -> we force a tactical pause
			APawnPlayerCamera::ApplyTimeDilationToWorld(0.f);
		else // we're closing the widget -> we remove the forced tactical pause
			APawnPlayerCamera::ApplyTimeDilationToWorld(1.f);
	}

	// if skill tree isn't visible (false) -> then animation(true), i.e. widget opening
	SkillTree->PlayOpenCloseAnimation(!bIsSkillTreeVisible);

	if (!bIsSkillTreeVisible) // from close to open
	{
		FadeTimeline->Play();
		
		FInputModeGameAndUI InputMode;
		// TakeWidget() is necessary in order to get the widget's underlying SWidget
		InputMode.SetWidgetToFocus(SkillTree->TakeWidget());
		InputMode.SetHideCursorDuringCapture(false);
		InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::LockOnCapture);
		CameraController->SetInputMode(InputMode);
	}
	else // from open to close
	{
		FadeTimeline->Reverse();

		FInputModeGameOnly InputMode;
		// Necessary to avoid a bug that blocks mouse input after closing the skill tree widget
		InputMode.SetConsumeCaptureMouseDown(false);
		CameraController->SetInputMode(InputMode);
	}

	bIsSkillTreeVisible = !bIsSkillTreeVisible;
}

void APawnPlayerCamera::SetSkillSlotsOpacity(float CurveValue) 
{
	for (USkillSlot* SkillSlot : SkillTree->GetSkillSlots())
		SkillSlot->SetRenderOpacity(CurveValue);
}