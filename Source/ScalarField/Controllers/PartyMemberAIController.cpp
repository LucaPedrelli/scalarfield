// Fill out your copyright notice in the Description page of Project Settings.


#include "PartyMemberAIController.h"
#include "ScalarField/Characters/PartyMember.h"
#include "ScalarField/Pawns/PawnPlayerCamera.h"

APartyMemberAIController::APartyMemberAIController() 
{
    
}

void APartyMemberAIController::BeginPlay() 
{
    Super::BeginPlay();

    GetWorldTimerManager().SetTimer(
        MoveTimerHandle, // FTimerHandler
        this, 
        &APartyMemberAIController::MoveToSelectedMember, // Function to call (delegate)
        MoveRate, // After how much time the function should be called
        true // looping
    );
}

void APartyMemberAIController::MoveToSelectedMember() 
{
    APawn* ControlledPartyMember = GetPawn();
    APartyMember* SelectedPartyMember = APawnPlayerCamera::GetSelectedPartyMember();
    if (ControlledPartyMember != SelectedPartyMember)
        MoveToActor(SelectedPartyMember, FMath::RandRange(100.f, 200.f));
}
