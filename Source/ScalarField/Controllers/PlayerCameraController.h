// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PlayerCameraController.generated.h"

/**
 * 
 */
UCLASS()
class SCALARFIELD_API APlayerCameraController : public APlayerController
{
	GENERATED_BODY()
	
};
