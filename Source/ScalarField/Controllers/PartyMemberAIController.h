// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "PartyMemberAIController.generated.h"

UCLASS()
class SCALARFIELD_API APartyMemberAIController : public AAIController
{
	GENERATED_BODY()

public:
	APartyMemberAIController();

protected:
	virtual void BeginPlay() override;

private:
	// ##### ATTRIBUTES #####
	FTimerHandle MoveTimerHandle;

	// How often we check if we need to adjust our world position based on the character selected
	// by the player. Measurment units: seconds.
	UPROPERTY(EditDefaultsOnly)
	float MoveRate = 1.f;

	// ##### FUNCTIONS #####
	void MoveToSelectedMember();	
};
