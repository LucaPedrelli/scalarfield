// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SkillTreeWidget.generated.h"

class APartyMember;
class UCanvasPanel;
class USkillSlot;
class UTextBlock;
class UWidgetAnimation;

UCLASS()
class SCALARFIELD_API USkillTreeWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	USkillTreeWidget(const FObjectInitializer & ObjectInitializer);

	virtual void NativeConstruct() override;

	void PlayOpenCloseAnimation(bool bIsOpen);

	void SetOwnerPartyMember(APartyMember* PartyMember);

	void UpdateSkillDescrTextBlockContent(const FString& SelectedSkillName, const FString& SelectedSkillDescr);
	void UpdateSkillpointsTextBlockContent();

	void RemoveRequirementFromSkillSlots(const FString& RequirmentToRemove, const TArray<FString>& TargetSlots);

	TArray<USkillSlot*> GetSkillSlots() const;

private:
	// ##### WIDGETS #####
	// By marking a pointer to a widget as BindWidget, we create an identically-named widget in a
	// Blueprint subclass of this C++ class, and at run-time access it from the C++.
	UPROPERTY(meta = (BindWidget))
	UTextBlock* SkillpointsTextBlock;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* SkillDescrTextBlock;

	UPROPERTY(meta = (BindWidget))
	UCanvasPanel* RootCanvasPanel;

	UPROPERTY(meta = (BindWidgetAnim))
	UWidgetAnimation* FadeAnimation;

	// ##### ATTRIBUTES #####
	APartyMember* OwnerPartyMember;

	TArray<USkillSlot*> SkillSlots;

	// ##### FUNCTIONS #####
	void UpdateSelectedPartyMember();
};
