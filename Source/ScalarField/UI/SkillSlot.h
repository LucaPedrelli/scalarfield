// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SkillSlot.generated.h"

class APartyMember;
class UButton;
class UImage;
class USkillTreeWidget;
class UTextBlock;
struct FSkillParams;

UCLASS()
class SCALARFIELD_API USkillSlot : public UUserWidget
{
	GENERATED_BODY()

public:
	USkillSlot(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	FString GetSkillName() const;

	void SetParentSkillTree(USkillTreeWidget* SkillTree);
	void SetOwnerPartyMember(APartyMember* PartyMember);
	void LoadSkillData(FSkillParams* SkillData);
	void RemoveSkillRequirement(const FString& RequirementToRemove);

private:
	// ##### ATTRIBUTES #####
	// ExposeOnSpawn=true is necessary to make the following properties editable on the instances
	// we'll add to the BP_SkillTreeWidget subclasses. ExposeOnSpawn=true requires
	// BlueprintReadWrite which, in turn, requires AllowPrivateAccess=true since we're in the
	// private section.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess=true, ExposeOnSpawn=true))
	FString Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess=true, ExposeOnSpawn=true))
	UObject* SkillIconTexture; 

	FString Description;
	int32 SkillpointsNeeded; // Number of skillpoints required to acquire the skill
	TArray<FString> BackwardSkills; // Skills that must be acquired before this one
	TArray<FString> ForwardSkills; // Skills that require this skill to be acquired
	bool bAcquired; // If the skill has been acquired or not by the character

	USkillTreeWidget* ParentSkillTree;

	APartyMember* OwnerPartyMember;

	// ##### WIDGETS #####
	UPROPERTY(meta = (BindWidget))
	UImage* FrameImage;

	UPROPERTY(meta = (BindWidget))
	UImage* SkillIconImage;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* SkillpointsNeededTextBlock;

	UPROPERTY(meta = (BindWidget))
	UButton* AcquireSkillButton;

	// ##### FUNCTIONS #####
	void UpdateSkillIcon();
	void UpdateFrameColor();
	void UpdateSkillpointsNeededTextBlock();
	UFUNCTION() // Only UFUNCTION() can be bound to delegates
	void SendNameAndDescriptionToTree();
	UFUNCTION() // Only UFUNCTION() can be bound to delegates
	void CleanTreeNameAndDescription();
	UFUNCTION()
	void UnlockSkill();
};
