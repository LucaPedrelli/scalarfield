// Fill out your copyright notice in the Description page of Project Settings.


#include "SkillSlot.h"
#include "Brushes/SlateImageBrush.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "ScalarField/Characters/PartyMember.h"
#include "ScalarField/UI/SkillTreeWidget.h"

USkillSlot::USkillSlot(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
    Name = "Name";
    Description = "Description";
    SkillpointsNeeded = 1;
    bAcquired = false;
}

void USkillSlot::NativeConstruct() 
{
    Super::NativeConstruct();

    UpdateSkillIcon();
    UpdateFrameColor();
    UpdateSkillpointsNeededTextBlock();
    
    AcquireSkillButton->OnHovered.AddDynamic(this, &USkillSlot::SendNameAndDescriptionToTree);
    AcquireSkillButton->OnUnhovered.AddDynamic(this, &USkillSlot::CleanTreeNameAndDescription);
    AcquireSkillButton->OnPressed.AddDynamic(this, &USkillSlot::UnlockSkill);
}

void USkillSlot::UpdateSkillIcon() 
{
    if (!SkillIconTexture)
        return;

    FVector2D ImageSize(32.f, 32.f);

    FSlateImageBrush ImageBrush(
        SkillIconTexture,
        ImageSize
    );

    SkillIconImage->SetBrush(ImageBrush);
}

void USkillSlot::UpdateFrameColor() 
{
    if (!FrameImage)
        return;
    
    FLinearColor FrameColor(1.f, 1.f, 1.f, 1.f); // White if locked;
    if (BackwardSkills.Num() == 0) // All previous skills have been acquired -> unlocked
    {
        if (bAcquired)
            FrameColor = FLinearColor(0.f, 1.f, 0.f, 1.f); // Green if acquired
        else
            FrameColor = FLinearColor(1.f, 1.f, 0.f, 1.f); // Yellow if unlocked but not acquired
    }
    FrameImage->SetColorAndOpacity(FrameColor);
}

void USkillSlot::UpdateSkillpointsNeededTextBlock() 
{
    SkillpointsNeededTextBlock->SetText(FText::FromString(FString::FromInt(SkillpointsNeeded)));
}

void USkillSlot::SetParentSkillTree(USkillTreeWidget* SkillTree) 
{
    ParentSkillTree = SkillTree;
}

void USkillSlot::SetOwnerPartyMember(APartyMember* PartyMember) 
{
    OwnerPartyMember = PartyMember;
}

void USkillSlot::LoadSkillData(FSkillParams* SkillData) 
{
    // Load character skill
    Description = SkillData->Description;
    SkillpointsNeeded = SkillData->SkillpointsNeeded;
    bAcquired = SkillData->bAcquired;
    BackwardSkills = SkillData->BackwardSkills;
    ForwardSkills = SkillData->ForwardSkills;
    UpdateFrameColor();
    UpdateSkillpointsNeededTextBlock();
}

void USkillSlot::SendNameAndDescriptionToTree() 
{
    if (ParentSkillTree)
        ParentSkillTree->UpdateSkillDescrTextBlockContent(Name, Description);
}

void USkillSlot::CleanTreeNameAndDescription() 
{
    if (ParentSkillTree)
        ParentSkillTree->UpdateSkillDescrTextBlockContent(TEXT(""), TEXT(""));
}

void USkillSlot::UnlockSkill() 
{
    if (!OwnerPartyMember)
        return;

    // All the previous skills in the tree have been acquired and this skill hasn't 
    if (BackwardSkills.Num() == 0 && !bAcquired)
    {
        int32 SkillpointsAvailable = OwnerPartyMember->GetSkillpointsAvailable();
        if (SkillpointsAvailable >= SkillpointsNeeded)
        {
            OwnerPartyMember->SetSkillpointsAvailable(SkillpointsAvailable - SkillpointsNeeded);
            bAcquired = true;
            UpdateFrameColor();
            ParentSkillTree->UpdateSkillpointsTextBlockContent();
            ParentSkillTree->RemoveRequirementFromSkillSlots(Name, ForwardSkills);
            // Save changes to character skill
            if (FSkillParams* SkillData = OwnerPartyMember->GetSkillsData().Find(Name))
            {
                SkillData->bAcquired = bAcquired;
            }
        }
    }
}

FString USkillSlot::GetSkillName() const
{
    return Name;
}

void USkillSlot::RemoveSkillRequirement(const FString& RequirementToRemove) 
{
    BackwardSkills.Remove(RequirementToRemove);
    // Save changes to character skill
    if (FSkillParams* SkillData = OwnerPartyMember->GetSkillsData().Find(Name))
    {
        SkillData->BackwardSkills = BackwardSkills;
    }
    UpdateFrameColor();
}