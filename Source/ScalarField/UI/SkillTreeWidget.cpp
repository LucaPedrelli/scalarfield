// Fill out your copyright notice in the Description page of Project Settings.


#include "SkillTreeWidget.h"
#include "Animation/WidgetAnimation.h"
#include "Components/CanvasPanel.h"
#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"
#include "ScalarField/Characters/PartyMember.h"
#include "ScalarField/Pawns/PawnPlayerCamera.h"
#include "ScalarField/UI/SkillSlot.h"

USkillTreeWidget::USkillTreeWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) 
{
    bIsFocusable = true;
}

void USkillTreeWidget::NativeConstruct() 
{
    Super::NativeConstruct();
    if (!RootCanvasPanel)
        return;
    
    for (UWidget* ChildWidget : RootCanvasPanel->GetAllChildren())
        if (USkillSlot* ChildSkillSlot = Cast<USkillSlot>(ChildWidget))
        {
            ChildSkillSlot->SetRenderOpacity(0.f);
            ChildSkillSlot->SetParentSkillTree(this);
            SkillSlots.Emplace(ChildSkillSlot);
        }
    
    UpdateSkillDescrTextBlockContent(TEXT(""), TEXT(""));
    SetIsEnabled(false);
}

void USkillTreeWidget::SetOwnerPartyMember(APartyMember* PartyMember)
{
    // We set the owning party member for both this tree and all its skill slots
    OwnerPartyMember = PartyMember;
    TMap<FString, FSkillParams>& OwnerSkills = OwnerPartyMember->GetSkillsData();
    for (USkillSlot* ChildSkillSlot : SkillSlots)
    {
        ChildSkillSlot->SetOwnerPartyMember(OwnerPartyMember);
        // For the skill slots, we load the data from the character
        if (FSkillParams* SkillData = OwnerSkills.Find(ChildSkillSlot->GetSkillName()))
        {
            ChildSkillSlot->LoadSkillData(SkillData);
        }
    }
    UpdateSkillpointsTextBlockContent();
}

void USkillTreeWidget::PlayOpenCloseAnimation(bool bIsOpening) 
{
    if (!FadeAnimation)
        return;

    UUserWidget::PlayAnimation(
        FadeAnimation,
        0.f,
        1,
        bIsOpening ? EUMGSequencePlayMode::Forward : EUMGSequencePlayMode::Reverse
    );
    SetIsEnabled(bIsOpening);
}

void USkillTreeWidget::UpdateSkillpointsTextBlockContent() 
{
    // SkillpointsTextBlock is nullptr if we haven't created it in the Blueprint subclass
	if (!SkillpointsTextBlock || !OwnerPartyMember)
        return;

    int32 Skillpoints = OwnerPartyMember->GetSkillpointsAvailable();
    FName PartyMemberName = OwnerPartyMember->GetName();
    FString SkillpointsText = PartyMemberName.ToString();
    SkillpointsText.Append(TEXT(": "));
    SkillpointsText.Append(FString::FromInt(Skillpoints));
    if (Skillpoints == 1)
        SkillpointsText.Append(TEXT(" skillpoint available"));
    else
        SkillpointsText.Append(TEXT(" skillpoints available"));

    SkillpointsTextBlock->SetText(FText::FromString(SkillpointsText));
}

void USkillTreeWidget::UpdateSkillDescrTextBlockContent(const FString& SelectedSkillName, const FString& SelectedSkillDescr) 
{
    if (!SkillDescrTextBlock)
        return;

    // Happens when the cursor isn't hoovering any skill slot in the skill tree
    if (SelectedSkillName == "" || SelectedSkillDescr == "")
    {
        SkillDescrTextBlock->SetText(FText::FromString(""));
        return;
    }

    FString SkillDescrText = TEXT("Skill name: ");
    SkillDescrText.Append(SelectedSkillName);
    SkillDescrText.Append(TEXT("\n\nSkill description: "));
    SkillDescrText.Append(SelectedSkillDescr);
    SkillDescrTextBlock->SetText(FText::FromString(SkillDescrText));
}

void USkillTreeWidget::RemoveRequirementFromSkillSlots(const FString& RequirmentToRemove, const TArray<FString>& TargetSlots) 
{
    for(const FString& TargetSlot : TargetSlots)
        for (USkillSlot* ChildSkillSlot : SkillSlots)
            if (TargetSlot == ChildSkillSlot->GetSkillName())
            {
                ChildSkillSlot->RemoveSkillRequirement(RequirmentToRemove);
                break;
            }
}

TArray<USkillSlot*> USkillTreeWidget::GetSkillSlots() const
{
    return SkillSlots;
}

