// Fill out your copyright notice in the Description page of Project Settings.


#include "PartyMember.h"
#include "ScalarField/UI/SkillTreeWidget.h"

APartyMember::APartyMember() 
{
    // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void APartyMember::SetName(const FName& Name) 
{
    PartyMemberName = Name;
}

void APartyMember::SetSkillpointsAvailable(int32 Skillpoints) 
{
    SkillpointsAvailable = Skillpoints;
}

FName APartyMember::GetName() const
{
    return PartyMemberName;
}

int32 APartyMember::GetSkillpointsAvailable() const
{
    return SkillpointsAvailable;
}

TSubclassOf<USkillTreeWidget> APartyMember::GetSkillTreeClass() const
{
    return PartyMemberSkillTreeClass;
}

TMap<FString, FSkillParams>& APartyMember::GetSkillsData()
{
    return SkillsData;
}
