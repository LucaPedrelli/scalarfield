// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterTemperature.h"
#include "ScalarField/Components/TemperatureComponent.h"

// Sets default values
ACharacterTemperature::ACharacterTemperature()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	TemperatureComp = CreateDefaultSubobject<UTemperatureComponent>(TEXT("Temperature"));

	// By default, temperature components have no mass: they are just a property that can be added to actors, pawns, and characters
	TemperatureComp->SetMassOverrideInKg(NAME_None, 0.f, true);

	TemperatureComp->SetupAttachment(RootComponent);
}

void ACharacterTemperature::BeginPlay() 
{
	Super::BeginPlay();
}

// Called to bind functionality to input
void ACharacterTemperature::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}