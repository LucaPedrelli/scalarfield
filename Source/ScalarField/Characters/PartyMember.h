// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ScalarField/Characters/CharacterTemperature.h"
#include "PartyMember.generated.h"

class USkillTreeWidget;

USTRUCT(BlueprintType) 
struct FPartyMemberParams {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Party member parameters")
	FName CharacterName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Party member parameters")
	FVector SpawnOffset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Party member parameters")
	int32 StartingSkillpoints;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Party member parameters")
	TSubclassOf<APartyMember> PartyMemberClass;
};

USTRUCT(BlueprintType)
struct FSkillParams {
	GENERATED_BODY()

public:
	// The name is not here because it's used as key in the TMap SkillsData

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Skill parameters")
	FString Description;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Skill parameters")
	int32 SkillpointsNeeded;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Skill parameters")
	TArray<FString> BackwardSkills; // Skills that must be acquired before this one
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Skill parameters")
	TArray<FString> ForwardSkills; // Skills that require this skill to be acquired

	bool bAcquired = false; // If the skill has been acquired or not by the character
};

UCLASS()
class SCALARFIELD_API APartyMember : public ACharacterTemperature
{
	GENERATED_BODY()
	
public:
	APartyMember();

	int32 GetSkillpointsAvailable() const;
	FName GetName() const;
	TSubclassOf<USkillTreeWidget> GetSkillTreeClass() const;
	TMap<FString, FSkillParams>& GetSkillsData();


	void SetName(const FName& Name);
	void SetSkillpointsAvailable(int32 Skillpoints);

private:
	UPROPERTY(VisibleAnywhere, Category="Party member info")
	FName PartyMemberName;

	UPROPERTY(VisibleAnywhere, Category="Party member info")
	int32 SkillpointsAvailable;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Class Skills", meta=(AllowPrivateAccess=true))
	TSubclassOf<USkillTreeWidget> PartyMemberSkillTreeClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Class Skills", meta=(AllowPrivateAccess=true))
	TMap<FString, FSkillParams> SkillsData;
};
