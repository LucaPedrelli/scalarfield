// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CharacterTemperature.generated.h"

class UTemperatureComponent;

UCLASS()
class SCALARFIELD_API ACharacterTemperature : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacterTemperature();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
private:
	// ##### COMPONENTS #####
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UTemperatureComponent* TemperatureComp;

	// ##### ATTRIBUTES #####
	UMaterialInstanceDynamic* MaterialInstance;
};
