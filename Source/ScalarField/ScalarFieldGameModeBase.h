// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ScalarFieldGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SCALARFIELD_API AScalarFieldGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
