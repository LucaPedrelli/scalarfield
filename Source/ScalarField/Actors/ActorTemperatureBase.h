// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ActorTemperatureBase.generated.h"

class UTemperatureComponent;

UCLASS()
class SCALARFIELD_API AActorTemperatureBase : public AActor
{
	GENERATED_BODY()
	
public:	
	AActorTemperatureBase();

	UTemperatureComponent* GetTemperatureComponent() const;

	UStaticMeshComponent* GetMesh() const;

private:
	// ##### COMPONENTS #####
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UTemperatureComponent* TemperatureComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Mesh;
};
