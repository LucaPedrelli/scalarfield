// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ActorTemperatureScatterer.generated.h"

class AActorTemperatureBase;
class UBoxComponent;
class UTemperatureComponent;

UCLASS()
class SCALARFIELD_API AActorTemperatureScatterer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AActorTemperatureScatterer();

	// OnConstruction is called when:
	// 1. When the actor is spawned
	// 2. When the actor is moved
	// 3. When a property of the actor is changed
	virtual void OnConstruction(const FTransform& Transform) override;

	void ToggleMoleculeVisibility();

protected:
	virtual void BeginPlay() override;

private:
	// ##### ATTRIBUTES #####
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Scatterer Attributes", meta = (AllowPrivateAccess = "true"))
	float Step = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Scatterer Attributes", meta = (AllowPrivateAccess = "true"))
	float BoxLenghtX = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Scatterer Attributes", meta = (AllowPrivateAccess = "true"))
	float BoxLenghtY = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Scatterer Attributes", meta = (AllowPrivateAccess = "true"))
	float BoxLenghtZ = 200.f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Scatterer Attributes", meta = (AllowPrivateAccess = "true"))
	int32 NActorsX = (BoxLenghtX / Step) + 1;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Scatterer Attributes", meta = (AllowPrivateAccess = "true"))
	int32 NActorsY = (BoxLenghtY / Step) + 1;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Scatterer Attributes", meta = (AllowPrivateAccess = "true"))
	int32 NActorsZ = (BoxLenghtZ / Step) + 1;

	TArray<AActorTemperatureBase*> AirMolecules;

	// Needed to specify everything about the molecules from Blueprints
	UPROPERTY(EditAnywhere, Category = "Molecules")
	TSubclassOf<AActorTemperatureBase> MoleculeClass;

	// ##### COMPONENTS #####
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	USceneComponent* SceneComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UBoxComponent* BoxComp;

	// ##### FUNCTIONS #####
	// Run OnConstruction to ensure that the scatterer dimensions allow molecules to be spawned with cubic lattice geometry
	void AdjustScattererAttributes();

	// Places a certain number of actors with UTemperatureComponents on them with cubic lattice geometry
	void MoleculesCubicScattering();

	// Computes the heat capacity that will be set for each molecule to obtain the best approximation of real air
	void ComputeMoleculeHeatCapacity();
};
