// Fill out your copyright notice in the Description page of Project Settings.


#include "ActorTemperatureScatterer.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "ScalarField/Actors/ActorTemperatureBase.h"
#include "ScalarField/Components/TemperatureComponent.h"

AActorTemperatureScatterer::AActorTemperatureScatterer()
{
 	// Set this actor to call Tick() every frame.
	// PrimaryActorTick.bCanEverTick = true;

	SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	RootComponent = SceneComp;

	BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
	BoxComp->SetupAttachment(RootComponent);
}

void AActorTemperatureScatterer::OnConstruction(const FTransform& Transform) 
{
	Super::OnConstruction(Transform);

	// Clamping values to be in acceptable ranges
	BoxLenghtX = BoxLenghtX > 0.f ? BoxLenghtX : 10.f;
	BoxLenghtY = BoxLenghtY > 0.f ? BoxLenghtY : 10.f;
	BoxLenghtZ = BoxLenghtZ > 0.f ? BoxLenghtZ : 10.f;
	Step = Step > 0.f ? Step : 10.f;

	AdjustScattererAttributes();
}

void AActorTemperatureScatterer::BeginPlay() 
{
	Super::BeginPlay();
	MoleculesCubicScattering();
}

void AActorTemperatureScatterer::AdjustScattererAttributes() 
{
	// The step can't be greater than any of the dimensions of the box
	if (Step > FMath::Min3<float>(BoxLenghtX, BoxLenghtY, BoxLenghtZ))
		Step = FMath::Min3<float>(BoxLenghtX, BoxLenghtY, BoxLenghtZ);

	// Adjusting the box dimensions so that they're the closest multiple of Step
	BoxLenghtX = static_cast<int32>(BoxLenghtX / Step) * Step;
	BoxLenghtY = static_cast<int32>(BoxLenghtY / Step) * Step;
	BoxLenghtZ = static_cast<int32>(BoxLenghtZ / Step) * Step;

	// Thanks to the adjustments above, NActorsX/Y/Z are now guaranteed to be int32s
    NActorsX = (BoxLenghtX / Step) + 1;
    NActorsY = (BoxLenghtY / Step) + 1;
    NActorsZ = (BoxLenghtZ / Step) + 1;

	if (!BoxComp)
		return;

	// Dividing by 2 because, with "extent", Unreal refers to the edge distance from center
	BoxComp->SetBoxExtent(FVector(BoxLenghtX / 2, BoxLenghtY / 2, BoxLenghtZ / 2), false);
	// This way the SceneComp is at the center of the bottom side of the box and it's easier to place it
	BoxComp->SetRelativeLocation(FVector(0.f, 0.f, BoxLenghtZ / 2), false, nullptr, ETeleportType::None);
}

void AActorTemperatureScatterer::MoleculesCubicScattering() 
{
	if (!MoleculeClass)
		return;

	UWorld* CurrentLevel = GetWorld();
	if (!CurrentLevel)
		return;

	FVector StartLocation = GetActorLocation() - FVector(BoxLenghtX / 2, BoxLenghtY / 2, 0.f);
	FVector SpawnLocation;

	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;
	AActorTemperatureBase* NewMolecule;
	for (int32 z = 0; z < NActorsZ; z++)
		for (int32 x = 0; x < NActorsX; x++)
			for (int32 y = 0; y < NActorsY; y++)
			{
				SpawnLocation = StartLocation + Step * FVector(x, y, z);
				NewMolecule = CurrentLevel->SpawnActor<AActorTemperatureBase>(
					MoleculeClass, 
					SpawnLocation, 
					FRotator::ZeroRotator, 
					SpawnParams
				);
				if (!NewMolecule)
					continue;
				NewMolecule->GetTemperatureComponent()->SetCapsuleSize(0.6f * Step, 0.6f * Step, true);
				AirMolecules.Emplace(NewMolecule);
			}
}

void AActorTemperatureScatterer::ToggleMoleculeVisibility() 
{
	UStaticMeshComponent* MoleculeMesh;
	for(AActorTemperatureBase* AirMolecule : AirMolecules)
	{
		MoleculeMesh = AirMolecule->GetMesh();
		// Toggle the mesh visibility
		MoleculeMesh->SetHiddenInGame(!MoleculeMesh->bHiddenInGame, true); 
	}
}
