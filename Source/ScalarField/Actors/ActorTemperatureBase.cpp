// Fill out your copyright notice in the Description page of Project Settings.


#include "ActorTemperatureBase.h"
#include "ScalarField/Components/TemperatureComponent.h"

AActorTemperatureBase::AActorTemperatureBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TemperatureComp = CreateDefaultSubobject<UTemperatureComponent>(TEXT("Temperature"));
	RootComponent = TemperatureComp;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);

	// By default, temperature components have no mass: they are just a property that can be added to actors, pawns, and characters
	TemperatureComp->SetMassOverrideInKg(NAME_None, 0.f, true);
}

UTemperatureComponent* AActorTemperatureBase::GetTemperatureComponent() const { return TemperatureComp; }

UStaticMeshComponent* AActorTemperatureBase::GetMesh() const { return Mesh; }
